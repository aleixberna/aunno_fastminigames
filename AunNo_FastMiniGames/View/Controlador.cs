﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace AunNo_FastMiniGames.View
{
    class Controlador
    {
        //static string cadenaConexion = @"data source=10.82.96.2;initial catalog=Empresa;user id=sa;password=tupassword;Connect Timeout=60";

        //@"Data Source=YourServerName\YourInstanceName;Initial Catalog=DatabaseName; User Id=XXXXX; Password=XXXXX";

        static string cadenaConexion = @"Data Source=oracle.ilerna.com;Initial Catalog=DAM2_BERNADOALEIX_MINIJUEGOS; User ID=DAM2_48253418Q;Password=xi8r5pep_";

        public int Ids { get; set; }
        public string Usuarios { get; set; }
        public int Puntuaciones { get; set; }
        public static List<Jugador> ObtenerJugadores()
        {
            List<Jugador> listaJugadores = new List<Jugador>();
            string sql = "SELECT * FROM RANKING ORDER BY puntuacion DESC";

            using (SqlConnection con = new SqlConnection(cadenaConexion))
            {
                con.Open();

                using (SqlCommand comando = new SqlCommand(sql, con))
                {
                    using (SqlDataReader reader = comando.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Jugador jugador = new Jugador()
                            {
                                Id = reader.GetInt32(0),
                                Usuario = reader.GetString(1),
                                Puntuacion = reader.GetInt32(2),
                            };

                            listaJugadores.Add(jugador);
                        }
                    }
                }

                con.Close();

                return listaJugadores;
            }
        }

        public static string AgregarJugador(string nom, int puntos)
        {
            int x = 0;
            int idfinal = 0;
            string mensaje = "conexion intentada";

            string sql1 = "SELECT TOP 1 id FROM ranking ORDER BY id DESC";

            using (SqlConnection con = new SqlConnection(cadenaConexion))
            {
                con.Open();
                mensaje = "conexion establecida";
                using (SqlCommand comando = new SqlCommand(sql1, con))
                {
                    comando.ExecuteNonQuery();
                    mensaje = "comando ejecutado";
                    using (SqlDataReader reader = comando.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            idfinal = reader.GetInt32(0);
                        }
                    }
                }

                con.Close();
            }

            idfinal = idfinal + 1;

            string sql2 = "INSERT INTO RANKING  (id , usuario, puntuacion) VALUES (" + idfinal + ",'" + nom + "', " + puntos + ");";
            
            using (SqlConnection con = new SqlConnection(cadenaConexion))
            {
                con.Open();
                mensaje = "conexion establecida";
                using (SqlCommand comando = new SqlCommand(sql2, con))
                {
                    comando.ExecuteNonQuery();
                    mensaje = "comando ejecutado";
                }

                con.Close();
            }
            
            return mensaje;
        }
    }
}


