﻿using AunNo_FastMiniGames.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AunNo_FastMiniGames.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Juego2 : ContentPage
    {
        public Juego2(object bindingContext)
        {
            InitializeComponent();
            BindingContext = bindingContext;
        }
        public Juego2()
        {
            InitializeComponent();
            BindingContext = new MainPageViewModel(this);
        }
        private void MainPage(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new MainPage(BindingContext));
        }
        private void j3_Inicio(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new j3_Inicio(BindingContext));
        }

    }
}