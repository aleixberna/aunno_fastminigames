﻿using AunNo_FastMiniGames.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AunNo_FastMiniGames.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Juego3 : ContentPage
    {
        public bool j3_InicioJuego = false;
        public bool j3_BFlecha1 = false;
        public bool j3_BFlecha2 = false;
        public bool j3_BFlecha3 = false;
        public Juego3()
        {
            InitializeComponent();
            BindingContext = new MainPageViewModel(this);
        }
        public Juego3(object bindingContext)
        {
            InitializeComponent();
            BindingContext = bindingContext;
        }
        private void MainPage(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new MainPage(BindingContext));
        }
        private void j4_Inicio(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new j4_Inicio(BindingContext));
        }
        
    }
}