﻿using AunNo_FastMiniGames.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AunNo_FastMiniGames.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Juego4 : ContentPage
    {
        private MainPageViewModel j4_mpvm4;
        private static int j4_Num1;
        public Juego4(object bindingContext)
        {
            j4_mpvm4 = new MainPageViewModel(this);
            InitializeComponent();
            BindingContext = bindingContext;
         
            Diffo();
            Diffo2();
        }
        public Juego4()
        {
            InitializeComponent();
            BindingContext = new MainPageViewModel(this);
        }

        public void Up(object sender, EventArgs e)
        {
            try
            {
                Up2();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public async Task Up2()
        {

            j4_mpvm4.j4_Y = j4_mpvm4.j4_Y - 10;

            await Tier.TranslateTo(j4_mpvm4.j4_X, j4_mpvm4.j4_Y);
            await Task.Delay(2);
        }
        public void Down(object sender, EventArgs e)
        {
            try
            {
                Down2();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public async Task Down2()
        {

            j4_mpvm4.j4_Y = j4_mpvm4.j4_Y + 10;

            await Tier.TranslateTo(j4_mpvm4.j4_X, j4_mpvm4.j4_Y);
            await Task.Delay(2);
        }

        public void Right(object sender, EventArgs e)
        {
            try
            {
                Right2();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public async Task Right2()
        {

            j4_mpvm4.j4_X = j4_mpvm4.j4_X - 10;

            await Tier.TranslateTo(j4_mpvm4.j4_X, j4_mpvm4.j4_Y);
            await Task.Delay(2);
        }

        //Funcion que mueve la imagen hacia izquierda
        public void Left(object sender, EventArgs e)
        {
            try
            {
                Left2();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public async Task Left2()
        {

            j4_mpvm4.j4_X = j4_mpvm4.j4_X + 10;

            await Tier.TranslateTo(j4_mpvm4.j4_X, j4_mpvm4.j4_Y);
            await Task.Delay(2);
        }

        public async Task Diffo()
        {
            //Esta funcion se encarga que el planeta se mueva en una direccion radom

            j4_Num1 = 5;

            while (true)
            {

                if (j4_mpvm4.j4_X >= 165 && j4_mpvm4.j4_Go == true)
                {
                    j4_mpvm4.j4_X = j4_mpvm4.j4_X + j4_Num1;

                }
                else if (j4_mpvm4.j4_X >= -165 && j4_mpvm4.j4_Go == true)
                {
                    j4_mpvm4.j4_X = j4_mpvm4.j4_X - j4_Num1;

                }



                await Tier.TranslateTo(j4_mpvm4.j4_X, j4_mpvm4.j4_Y);
                await Task.Delay(300);
            }
        }

        public async Task Diffo2()
        {
            //Esta funcion se encarga que el planeta se mueva en una direccion radom

            j4_Num1 = 5;

            while (true)
            {

                if (j4_mpvm4.j4_Y >= 1 && j4_mpvm4.j4_Go == true)
                {
                    j4_mpvm4.j4_Y = j4_mpvm4.j4_Y + j4_Num1;

                }
                else if (j4_mpvm4.j4_Y >= 0 && j4_mpvm4.j4_Go == true)
                {
                    j4_mpvm4.j4_Y = j4_mpvm4.j4_Y - j4_Num1;

                }

                await Tier.TranslateTo(j4_mpvm4.j4_X, j4_mpvm4.j4_Y);
                await Task.Delay(300);
            }
        }


        private void j5_Inicio(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new j5_Inicio(BindingContext));
        }
    }
}