﻿using AunNo_FastMiniGames.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AunNo_FastMiniGames.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Juego5 : ContentPage
    {
        public Juego5()
        {
            InitializeComponent();
            BindingContext = new MainPageViewModel(this);
        }
        public Juego5(object bindingContext)
        {
            InitializeComponent();
            BindingContext = bindingContext;
        }
        private void MainPage(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new MainPage(BindingContext));
        }
        private void j6_Inicio(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new j6_Inicio(BindingContext));
        }
       
    }
}