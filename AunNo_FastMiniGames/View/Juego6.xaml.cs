﻿using AunNo_FastMiniGames.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AunNo_FastMiniGames.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Juego6 : ContentPage
    {
        private MainPageViewModel j6_MPVM;

        public int j6_Velocidad = 90;
        public int j6_Puntos = 0;
        public int j6_Ronda = 0;
        private int j6_Mov_Perro = 25;
        private Boolean j6_Stop = true;
        private Boolean j6_Dir_Op = true;
        public Boolean j6_Continuar = false;
        public Juego6()
        {
            InitializeComponent();
            BindingContext = j6_MPVM;
            j6_MPVM = new MainPageViewModel(this);
            BackgroundImageSource = "j6_Fondo.png";
            j6_Move();
        }
        public Juego6(object bindingContext)
        {
            InitializeComponent();
            BindingContext = bindingContext;
        }

        public void j6_Down(object sender, EventArgs e)
        {
            try
            {
                j6_Down2();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public void j6_Volver(object sender, EventArgs e)
        {
            try
            {
                j6_Volver2();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public async Task j6_Down2()
        {
            //Funcion encargada de dejar caer la jaula, de puntuar en caso de que el perro haya sido capturado,y despues de dejar caer la jaula llama a la funcion "Reset"
            j6_Switch.Source = "j6_on.png";
            while (j6_Stop)
            {
                j6_MPVM.j6_Y = j6_MPVM.j6_Y + 25;

                await j6_Jaula.TranslateTo(j6_MPVM.j6_X, j6_MPVM.j6_Y);
                await Task.Delay(5);

                if (j6_MPVM.j6_Y > -200)
                {
                    j6_Stop = false;
                    j6_Continuar = true;
                    j6_Mov_Perro = 0;
                    //j6_Wolf.IsAnimationPlaying = false;

                    if (j6_MPVM.j6_Xp >= 0 && j6_MPVM.j6_Xp <= 175)
                    {
                        j6_Puntos = j6_Puntos + 300;

                    }

                    j6_Reset();
                }

            }
        }
        public async Task j6_Move()
        {
            //Funcion encargada del movimiento del lobo y de las animaciones de girar cuando llega al extremo de la pantalla
            j6_MPVM.j6_X = 90;
            j6_MPVM.j6_Y = -930;
            await j6_Jaula.TranslateTo(j6_MPVM.j6_X, j6_MPVM.j6_Y);

            while (true)
            {
                if (j6_MPVM.j6_Xp <= 395 && j6_Dir_Op)
                {
                    j6_MPVM.j6_Xp = j6_MPVM.j6_Xp + j6_Mov_Perro;
                    if (j6_MPVM.j6_Xp >= 395)
                    {
                        j6_Wolf.RotationY = 180;
                        j6_Dir_Op = false;
                    }
                }

                else if (j6_MPVM.j6_Xp >= -300 && j6_Dir_Op == false)
                {
                    j6_MPVM.j6_Xp = j6_MPVM.j6_Xp - j6_Mov_Perro;

                    if (j6_MPVM.j6_Xp <= -300)
                    {
                        j6_Wolf.RotationY = 0;
                        j6_Dir_Op = true;
                    }
                }

                await j6_Wolf.TranslateTo(j6_MPVM.j6_Xp, j6_MPVM.j6_Yp);

                await Task.Delay(j6_Velocidad);
            }
        }
        public async Task j6_Reset()
        {

            //Funcion que llama a un "popup" que muestra los Puntos y numero de Ronda , resetea y altera variables
            while (j6_Continuar == true)
            {
                if (j6_Ronda <= 1)
                {
                    j6_Ronda = j6_Ronda + 1;
                    j6_Switch.Source = "j6_off.png";
                    j6_PopPuntos.IsVisible = true;
                    j6_PopPuntos.Text = "Ronda " + j6_Ronda.ToString() + "\n Puntuacion: " + j6_Puntos.ToString();
                    j6_Menu.IsVisible = true;
                    j6_Button.IsVisible = true;
                    j6_Switch.IsVisible = false;
                    j6_Wolf.IsVisible = false;
                    j6_Jaula.IsVisible = false;
                    j6_MPVM.j6_Y = -930;
                    await j6_Jaula.TranslateTo(j6_MPVM.j6_X, j6_MPVM.j6_Y);
                    j6_MPVM.j6_Xp = 0;
                    await j6_Wolf.TranslateTo(j6_MPVM.j6_Xp, j6_MPVM.j6_Yp);
                    j6_Mov_Perro = 25;
                    j6_Velocidad = j6_Velocidad - 30;
                    j6_Continuar = false;
                }
                else if (j6_Ronda <= 2)
                {
                    BackgroundImageSource = "j6_Fondo_Pausa.png";
                    j6_Ronda = j6_Ronda + 1;
                    j6_PopPuntos.IsVisible = true;
                    j6_PopPuntos.Text = "Ronda " + j6_Ronda.ToString() + "\n Puntuacion " + j6_Puntos.ToString();
                    j6_Menu.IsVisible = true;
                    j6_Button.IsVisible = true;
                    j6_Switch.IsVisible = false;
                    j6_Wolf.IsVisible = false;
                    j6_Jaula.IsVisible = false;
                    j6_Continuar = false;
                }
            }
            await Task.Delay(5);
        }
        public async Task j6_Volver2()
        {
            //Funcion encargada de la visibilidad de los elementos despues de la ventana de la funcion "Reset"
            if (j6_Ronda <= 2)
            {
                j6_Stop = true;
                j6_Menu.IsVisible = false;
                j6_Button.IsVisible = false;
                j6_PopPuntos.IsVisible = false;
                j6_Switch.IsVisible = true;
                j6_Wolf.IsVisible = true;
                j6_Jaula.IsVisible = true;
                //j6_Wolf.IsAnimationPlaying = true;
            }
            else if (j6_Ronda <= 3)
            {
                //Aqui tendria que llamar a la funcion para pasar al siguiente juego ya que se habrian jugado las 3 rondas
            }
            await Task.Delay(1);
        }

        private void MainPage(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new MainPage(BindingContext));
        }
        private void j7_Inicio(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new j7_Inicio(BindingContext));
        }
    }
}