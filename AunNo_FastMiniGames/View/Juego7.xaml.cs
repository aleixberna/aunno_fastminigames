﻿using AunNo_FastMiniGames.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AunNo_FastMiniGames.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Juego7 : ContentPage
    {
        public Juego7()
        {
            InitializeComponent();
            BindingContext = new MainPageViewModel(this);
        }
        public Juego7(object bindingContext)
        {
            InitializeComponent();
            BindingContext = bindingContext;
        }
        private void MainPage(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new MainPage(BindingContext));
        }
        private void Juego8(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new Juego8(BindingContext));
        }
        private void Login(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new Login(BindingContext));
        }

    }
}