﻿using AunNo_FastMiniGames.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AunNo_FastMiniGames.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Juego8 : ContentPage
    {
        public Juego8()
        {
            InitializeComponent();
            BindingContext = new MainPageViewModel(this);
        }
        public Juego8(object bindingContext)
        {
            InitializeComponent();
            BindingContext = bindingContext;
        }
        private void MainPage(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new MainPage(BindingContext));
        }
        private void Juego1(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new Juego1(BindingContext));
        }
        private void Juego2(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new Juego2(BindingContext));
        }
        private void Juego3(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new Juego3(BindingContext));
        }
        private void Juego4(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new Juego4(BindingContext));
        }
        private void Juego5(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new Juego5(BindingContext));
        }
        private void Juego6(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new Juego6(BindingContext));
        }
    }
}