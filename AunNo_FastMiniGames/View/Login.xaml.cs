﻿using AunNo_FastMiniGames.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AunNo_FastMiniGames.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Login : ContentPage
    {
        public Login(object bindingContext)
        {
            InitializeComponent();
            BindingContext = bindingContext;
        }
        public Login()
        {
            InitializeComponent();
            BindingContext = new MainPageViewModel(this);

        }
        private void MainPage(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new MainPage(BindingContext));
        }
        private void Ranking(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new Ranking(BindingContext));
        }
    }
}