﻿using AunNo_FastMiniGames.View;
using AunNo_FastMiniGames.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AunNo_FastMiniGames
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage(object bindingContext)
        {
            InitializeComponent();
            BindingContext = bindingContext;
        }
        public MainPage()
        {
            InitializeComponent();
            BindingContext = new MainPageViewModel(this);

        }
        private void j1_Inicio(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new j1_Inicio(BindingContext));
        }
        private void Informacion(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new Informacion(BindingContext));
        }
        private void Ranking(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new Ranking(BindingContext));
        }

    }
}
