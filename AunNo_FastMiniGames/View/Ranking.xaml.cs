﻿using AunNo_FastMiniGames.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AunNo_FastMiniGames.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Ranking : ContentPage
    {
        public Ranking(object bindingContext)
        {
            InitializeComponent();
            BindingContext = bindingContext;

            List<Jugador> listaJugadores = new List<Jugador>();

            listaJugadores = Controlador.ObtenerJugadores();

            string name1 = "";
            string name2 = "";
            string num1 = "";
            string num2 = "";
            string punt1 = "";
            string punt2 = "";
            int cont = 1;

            foreach (Jugador j in listaJugadores)
            {
                name1 = j.Usuario + "\n";
                name2 = name2 + name1;

            }
            nombre.Text = name2;

            foreach (Jugador i in listaJugadores)
            {
                num1 = cont + "\n";
                num2 = num2 + num1;
                cont++;

            }
            ides.Text = num2;

            foreach (Jugador z in listaJugadores)
            {
                punt1 = z.Puntuacion + "\n";
                punt2 = punt2 + punt1;

            }
            puntua.Text = punt2;

        }
        public Ranking()
        {
            InitializeComponent();
            BindingContext = new MainPageViewModel(this);

        }
        private void MainPage(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new MainPage(BindingContext));
        }
    }
}