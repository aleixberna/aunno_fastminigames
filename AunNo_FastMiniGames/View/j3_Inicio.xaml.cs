﻿using AunNo_FastMiniGames.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AunNo_FastMiniGames.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class j3_Inicio : ContentPage
    {
        public j3_Inicio(object bindingContext)
        {
            InitializeComponent();
            BindingContext = bindingContext;

        }
        public j3_Inicio()
        {
            InitializeComponent();
            BindingContext = new MainPageViewModel(this);

        }

        private void MainPage(object sender, EventArgs e)
        {
            InitializeComponent();
            this.Navigation.PushModalAsync(new MainPage(BindingContext));

        }
        private void Juego3(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new Juego3(BindingContext));
        }
    }
}