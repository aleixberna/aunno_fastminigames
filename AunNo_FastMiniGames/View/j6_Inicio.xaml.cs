﻿using AunNo_FastMiniGames.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AunNo_FastMiniGames.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class j6_Inicio : ContentPage
    {
        public j6_Inicio(object bindingContext)
        {
            InitializeComponent();
            BindingContext = bindingContext;

        }
        public j6_Inicio()
        {
            InitializeComponent();
            BindingContext = new MainPageViewModel(this);

        }

        private void MainPage(object sender, EventArgs e)
        {
            InitializeComponent();
            this.Navigation.PushModalAsync(new MainPage(BindingContext));

        }
        private void Juego6(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new Juego6(BindingContext));
        }
    }
}