﻿using AunNo_FastMiniGames.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AunNo_FastMiniGames.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class j7_Inicio : ContentPage
    {
        public j7_Inicio(object bindingContext)
        {
            InitializeComponent();
            BindingContext = bindingContext;

        }
        public j7_Inicio()
        {
            InitializeComponent();
            BindingContext = new MainPageViewModel(this);

        }

        private void MainPage(object sender, EventArgs e)
        {
            InitializeComponent();
            this.Navigation.PushModalAsync(new MainPage(BindingContext));

        }
        private void Juego7(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new Juego7(BindingContext));
        }
    }
}