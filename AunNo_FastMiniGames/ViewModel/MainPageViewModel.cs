﻿using AunNo_FastMiniGames.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace AunNo_FastMiniGames.ViewModel
{
    public class MainPageViewModel : ContentPage, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        CultureInfo cultureInfo = CultureInfo.CreateSpecificCulture("el-GR");

        ContentPage nav;

        public ICommand j1_ComandoGo { get; set; }
        public ICommand j1_CespedC { get; set; }
        public ICommand j1_OvejaC { get; set; }
        public ICommand j1_LoboC { get; set; }
        public ICommand j2_BotoLeft1 { get; set; }
        public ICommand j2_BotoLeft2 { get; set; }
        public ICommand j2_BotoLeft3 { get; set; }
        public ICommand j2_BotoLeft4 { get; set; }
        public ICommand j2_BotoLeft5 { get; set; }
        public ICommand j2_BotoCentro { get; set; }
        public ICommand j2_BotoRight1 { get; set; }
        public ICommand j2_BotoRight2 { get; set; }
        public ICommand j2_BotoRight3 { get; set; }
        public ICommand j2_BotoRight4 { get; set; }
        public ICommand j2_BotoRight5 { get; set; }
        public ICommand j2_BotoReset { get; set; }
        public ICommand j3_BotonJugar { get; set; }
        public ICommand j3_CBolsa1 { get; set; }
        public ICommand j3_CBolsa2 { get; set; }
        public ICommand j3_CBolsa3 { get; set; }
        public ICommand j3_CPaleta1 { get; set; }
        public ICommand j3_CPaleta2 { get; set; }
        public ICommand j3_CPaleta3 { get; set; }
        public ICommand j5_ComandoSi { get; set; }
        public ICommand j5_ComandoNo { get; set; }
        public ICommand j5_Comando_Colores { get; set; }
        public ICommand j7_BotoCarta1 { get; set; }
        public ICommand j7_BotoCarta2 { get; set; }
        public ICommand j7_BotoCarta3 { get; set; }
        public ICommand j7_BotoCarta4 { get; set; }
        public ICommand j7_BotoCarta5 { get; set; }
        public ICommand j7_BotoCarta6 { get; set; }
        public ICommand j7_BotoCarta7 { get; set; }
        public ICommand j7_BotoCarta8 { get; set; }
        public ICommand j7_BotoCarta9 { get; set; }
        public ICommand j7_BotoCarta10 { get; set; }
        public ICommand j7_BotoCarta11 { get; set; }
        public ICommand j7_BotoCarta12 { get; set; }
        public ICommand j7_BotoReiniciar { get; set; }
        public ICommand jr_cNom { get; set; }
        

        //--------------------------------------- VARIABLES INT ---------------------------------------//
        //----------- JUEGO 1 -----------//
        public int j1_tiempo = 40;
        public int j1_puntos = 0;
        public int j1_puntos2 = 0;
        public int j1_Barcodir = 0;
        public int j1_cespedx = 210;
        public int j1_ovejax = 265;
        public int j1_lobox = 320;
        public int j1_barcox = 100;
        public int j1_lobo = 0;
        public int j1_cesped = 0;
        public int j1_oveja = 0;
        //----------- JUEGO 2 -----------//
        public int j2_numeroArray = 0;
        public int j2_i = 0;
        public int j2_z = 99;
        public int j2_numClicks = 50;
        public int j2_pvosivcioEnable1 = 0;
        public int j2_posicioEnable2 = 0;
        public int j2_posicioEnable3 = 0;
        public int j2_posicioEnable4 = 0;
        public int j2_posicioBuida = 0;
        public int j2_puntosgl = 40;
        public int j2_Punts = 0;
        //----------- JUEGO 3 -----------//
        public static int j3_solucion;
        public int j3_Punts = 0;
        public int j3_Tiempo = 30;
        //----------- JUEGO 4 -----------//
        public int j4_x = -155;
        public int j4_y = 0;
        public int j4_seg = 0;
        public int j4_min = 0;
        public static int j4_puntos = 0;
        public static int j4_puntosF = 0;
        public static int j4_tiempoF = 30;
        //----------- JUEGO 5 -----------//
        public int j5_rc = 0;
        public int j5_rt = 0;
        public int j5_puntos = 0;
        public int j5_sincro = 0;
        public int j5_si = 0;
        public int j5_no = 0;
        public int j5_contador_afir = 0;
        //----------- JUEGO 6 -----------//
        public int j6_yp = 105;
        public int j6_xp = 0;
        public int j6_y = -930;
        public int j6_x = 90;
        public int j6_seg = -2;
        public int j6_min = 0;
        //----------- JUEGO 7 -----------//
        public int j7_nivel = 4;
        public int j7_marcado = 0;
        public int j7_numeroClick = 0;
        public int j7_contgirades = 0;
        public int j7_numrandom = 0;
        public int j7_posrandom = 0;
        public int j7_posrandom2 = 0;
        public int j7_puntuacio = 0;
        public int j7_puntuacio_flag = 0;
        public int j7_tiempo = 20;
        //----------- RANKING -----------//
        public int jr_puntos_ranking;

        //------------------------------------ VARIABLES STRING -----------------------------------//
        //----------- JUEGO 2 -----------//
        public String j2_left1 = "j2_blava1.png";
        public String j2_left2 = "j2_blava2.png";
        public String j2_left3 = "j2_blava3.png";
        public String j2_left4 = "j2_blava4.png";
        public String j2_left5 = "j2_blava5.png";
        public String j2_centro = "j2_vacio.png";
        public String j2_right1 = "j2_verd1.png";
        public String j2_right2 = "j2_verd2.png";
        public String j2_right3 = "j2_verd3.png";
        public String j2_right4 = "j2_verd4.png";
        public String j2_right5 = "j2_verd5.png";
        public String j2_flagcarta = "";
        public String j2_imagenFondo = "j2_pantano.png";
        //----------- JUEGO 3 -----------//
        public String j3_bolsaAyuda1 = "";
        public String j3_bolsaAyuda2 = "";
        public String j3_bolsaAyuda3 = "";
        public String j3_visPaleta1 = "";
        public String j3_visPaleta2 = "";
        public String j3_visPaleta3 = "";
        public String j3_textoJuego = "Bienvenido, pulsa START para empezar!";
        public String paleta1 = "j3_CartellBoliBoli.png";
        public String paleta2 = "j3_CartellBoliBoli.png";
        public String paleta3 = "j3_CartellBoliBoli.png";
        public String j3_bolsa1 = "j3_BolsaNormal.png";
        public String j3_bolsa2 = "j3_BolsaBoliNada.png";
        public String j3_bolsa3 = "j3_BolsaNormal.png";
        public String j3_bEnabled1 = "True";
        public String j3_bEnabled2 = "True";
        public String j3_bEnabled3 = "True";
        //----------- JUEGO 4 -----------//
        public String j4_timer = "0:00";
        public String j4_puntuacion = "j4_Puntuacion:0";
        //----------- JUEGO 5 -----------//
        public String j5_color1;
        public String j5_text_color;
        public String j5_botoSi;
        public String j5_botoNo;
        //----------- JUEGO 6 -----------//
        public String j6_timer = "0:00";
        //----------- JUEGO 7 -----------//
        public String j7_flagfotos = "null";
        public String j7_valorar1 = "null";
        public String j7_valorar2 = "null";
        public String j7_textMostrar = "MEMORY";
        public String j7_imatgeCarta1 = "j7_cartareves.png";
        public String j7_imatgeCarta2 = "j7_cartareves.png";
        public String j7_imatgeCarta3 = "j7_cartareves.png";
        public String j7_imatgeCarta4 = "j7_cartareves.png";
        public String j7_imatgeCarta5 = "j7_cartareves.png";
        public String j7_imatgeCarta6 = "j7_cartareves.png";
        public String j7_imatgeCarta7 = "j7_cartareves.png";
        public String j7_imatgeCarta8 = "j7_cartareves.png";
        public String j7_imatgeCarta9 = "j7_cartareves.png";
        public String j7_imatgeCarta10 = "j7_cartareves.png";
        public String j7_imatgeCarta11 = "j7_cartareves.png";
        public String j7_imatgeCarta12 = "j7_cartareves.png";
        public String j7_imatgeRandom1 = "j7_cartareves.png";
        public String j7_imatgeRandom2 = "j7_cartareves.png";
        public String j7_imatgeRandom3 = "j7_cartareves.png";
        public String j7_imatgeRandom4 = "j7_cartareves.png";
        //----------- RANKING -----------//
        public String jr_nombre;

        //------------------------------------ VARIABLES BOOLEAN -----------------------------------//
        //----------- JUEGO 1 -----------//
        public Boolean j1_ganada = false;
        public Boolean j1_posB = false;
        public Boolean j1_barcoUnlock = true;
        public Boolean j1_goaa = true;
        public Boolean j1_gobb = false;
        public Boolean j1_cespedlock = false;
        public Boolean j1_ovejalock = false;
        public Boolean j1_lobolock = false;
        public Boolean j1_ovejapos = false;
        public Boolean j1_cespedpos = false;
        public Boolean j1_lobopos = false;
        public Boolean j1_warning = false;
        public Boolean j1_chivato_botones = true;
        public Boolean j1_tiempobool = false;
        //----------- JUEGO 2 -----------//
        public Boolean j2_left1Enable = false;
        public Boolean j2_left2Enable = false;
        public Boolean j2_left3Enable = false;
        public Boolean j2_left4Enable = true;
        public Boolean j2_left5Enable = true;
        public Boolean j2_centroEnable = false;
        public Boolean j2_right1Enable = true;
        public Boolean j2_right2Enable = true;
        public Boolean j2_right3Enable = false;
        public Boolean j2_right4Enable = false;
        public Boolean j2_right5Enable = false;
        public Boolean j2_flagbooluna = true;
        public Boolean j2_visibleReset = false;
        public Boolean j2_tiempobool = false;
        //----------- JUEGO 3 -----------//
        public Boolean j3_bolsa1Cliked = false;
        public Boolean bolsa2Cliked = false;
        public Boolean bolsa3Cliked = false;
        public Boolean j3_paleta1Cliked = false;
        public Boolean j3_paleta2Cliked = false;
        public Boolean j3_paleta3Cliked = false;
        public Boolean j3_case1 = false;
        public Boolean j3_case2 = false;
        public Boolean j3_case3 = false;
        public Boolean j3_ccase1 = false;
        public Boolean j3_ccase2 = false;
        public Boolean j3_ccase3 = false;
        public Boolean j3_donecase1 = false;
        public Boolean j3_donecase2 = false;
        public Boolean j3_donecase3 = false;
        public Boolean j3_primeraVez = false;
        public Boolean j3_paleta = false;
        public Boolean j3_bolsa = false;
        public Boolean j3_BBB = false;
        public Boolean j3_BBL = false;
        public Boolean j3_BLL = false;
        public Boolean j3_PBB = false;
        public Boolean j3_PBL = false;
        public Boolean j3_PLL = false;
        public Boolean j3_PF1 = false;
        public Boolean j3_PF2 = false;
        public Boolean j3_PF3 = false;
        public Boolean BP1 = false;
        public Boolean BP2 = false;
        public Boolean BP3 = false;
        public Boolean BB1 = false;
        public Boolean BB2 = false;
        public Boolean BB3 = false;
        public Boolean OK1 = false;
        public Boolean OK2 = false;
        public Boolean OK3 = false;
        public Boolean j3_Clicked = false;
        //----------- JUEGO 4 -----------//
        public Boolean j4_go = true;
        public Boolean j4_stop = true;
        public Boolean j4_tiempobool = false;
        //----------- JUEGO 5 -----------//
        public Boolean j5_contestada = true;
        public Boolean j5_conta_afir = false;
        public Boolean j5_tiempobool = false;
        //----------- JUEGO 7 -----------//
        public Boolean j7_estatcarta1 = true;
        public Boolean j7_estatcarta2 = true;
        public Boolean j7_estatcarta3 = true;
        public Boolean j7_estatcarta4 = true;
        public Boolean j7_estatcarta5 = true;
        public Boolean j7_estatcarta6 = true;
        public Boolean j7_estatcarta7 = true;
        public Boolean j7_estatcarta8 = true;
        public Boolean j7_estatcarta9 = true;
        public Boolean j7_estatcarta10 = true;
        public Boolean j7_estatcarta11 = true;
        public Boolean j7_estatcarta12 = true;
        public Boolean j7_activarCarta1 = true;
        public Boolean j7_activarCarta2 = true;
        public Boolean j7_activarCarta3 = true;
        public Boolean j7_activarCarta4 = true;
        public Boolean j7_activarCarta5 = true;
        public Boolean j7_activarCarta6 = true;
        public Boolean j7_activarCarta7 = true;
        public Boolean j7_activarCarta8 = true;
        public Boolean j7_activarCarta9 = true;
        public Boolean j7_activarCarta10 = true;
        public Boolean j7_activarCarta11 = true;
        public Boolean j7_activarCarta12 = true;
        public Boolean j7_cartaVisible1 = false;
        public Boolean j7_cartaVisible2 = false;
        public Boolean j7_cartaVisible3 = false;
        public Boolean j7_cartaVisible4 = false;
        public Boolean j7_flag = true;
        public Boolean j7_visibleReiniciar = false;
        public Boolean j7_tiempobool = false;
        //----------- RANKING -----------//
        public Boolean jr_vrank = false;
        public Boolean jr_vmenu = true;
        public Boolean pasarJuego1 = false;
        public Boolean pasarJuego2 = false;
        public Boolean pasarJuego3 = false;
        public Boolean pasarJuego4 = false;
        public Boolean pasarJuego5 = false;
        public Boolean pasarJuego6 = false;
        public Boolean pasarJuego7 = false;
        public Boolean pasarJuego8 = false;

        //----------------------------------------- ARRAYS I RANDOMS ----------------------------------------//
        //----------- JUEGO 2 -----------//
        String[] j2_ArrayRanas = new String[11];
        Boolean[] j2_ArrayRanasBool = new Boolean[11];
        //----------- JUEGO 4 -----------//
        Random j4_randum = new Random();
        //----------- JUEGO 7 -----------//
        String[] j7_ArrayCartes = new String[12];
        String [] j7_ArrayFotos = new String[19];
        
        //----------------------------------- GETTERS I SETTERS -----------------------------------//
        //----------- JUEGO 1 -----------//
        public int j1_Tiempo
        { 
            set 
            { 
                if (j1_tiempo != value) 
                { 
                    j1_tiempo = value; 
                    if (PropertyChanged != null) 
                    { 
                        PropertyChanged(this, new PropertyChangedEventArgs("j1_Tiempo")); 
                    } 
                } 
            } 
            get { return j1_tiempo; } 
        }
        public int j1_Puntos
        { 
            set 
            { 
                if (j1_puntos != value) 
                { 
                    j1_puntos = value; 
                    if (PropertyChanged != null) 
                    { 
                        PropertyChanged(this, new PropertyChangedEventArgs("j1_Puntos")); 
                    } 
                } 
            } 
            get { return j1_puntos; } 
        }
        public int j1_Barcox
        { 
            set 
            { 
                if (j1_barcox != value) 
                { 
                    j1_barcox = value; 
                    if (PropertyChanged != null) 
                    { 
                        PropertyChanged(this, new PropertyChangedEventArgs("j1_Barcox")); 
                    } 
                } 
            } 
            get { return j1_barcox; } 
        }
        public int j1_Lobox
        { 
            set 
            { 
                if (j1_lobox != value) 
                { 
                    j1_lobox = value; 
                    if (PropertyChanged != null) 
                    { 
                        PropertyChanged(this, new PropertyChangedEventArgs("j1_Lobox")); 
                    } 
                } 
            }
            get { return j1_lobox; } 
        }
        public int j1_Cespedx
        { 
            set 
            { 
                if (j1_cespedx != value) 
                { 
                    j1_cespedx = value; 
                    if (PropertyChanged != null) 
                    { 
                        PropertyChanged(this, new PropertyChangedEventArgs("j1_Cespedx")); 
                    } 
                }
            }
            get { return j1_cespedx; } 
        }
        public int j1_Ovejax
        { 
            set 
            { 
                if (j1_ovejax != value) 
                { 
                    j1_ovejax = value; 
                    if (PropertyChanged != null) 
                    { 
                        PropertyChanged(this, new PropertyChangedEventArgs("j1_Ovejax")); 
                    } 
                }
            }
            get { return j1_ovejax; } 
        }
        public Boolean j1_BarcoUnlock
        { 
            set 
            { 
                if (j1_barcoUnlock != value) 
                { 
                    j1_barcoUnlock = value; 
                    if (PropertyChanged != null) 
                    { 
                        PropertyChanged(this, new PropertyChangedEventArgs("j1_BarcoUnlock")); 
                    }
                }
            }
            get { return j1_barcoUnlock; } 
        }  
        public Boolean j1_Warning
        { 
            set 
            { 
                if (j1_warning != value) 
                { 
                    j1_warning = value; 
                    if (PropertyChanged != null) 
                    { 
                        PropertyChanged(this, new PropertyChangedEventArgs("j1_Warning")); 
                    }
                }
            }
            get { return j1_warning; }
        }
        public Boolean j1_Chivato_botones
        {
            set 
            {
                if (j1_chivato_botones != value) 
                {
                    j1_chivato_botones = value; 
                    if (PropertyChanged != null) 
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j1_Chivato_botones")); 
                    }
                }
            }
            get { return j1_chivato_botones; } 
        }
        //----------- JUEGO 2 -----------//
        public int j2_Puntosgl
        {
            set
            {
                if (j2_puntosgl != value)
                {
                    j2_puntosgl = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j2_Puntosgl"));
                    }
                }
            }
            get { return j2_puntosgl; }
        }
        public int j2_NumClicks
        {
            set
            {
                if (j2_numClicks != value)
                {
                    j2_numClicks = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j2_NumClicks"));
                    }
                }
            }
            get { return j2_numClicks; }
        }
        public String j2_Left1
        {
            set
            {
                if (j2_left1 != value)
                {
                    j2_left1 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j2_Left1"));
                    }
                }
            }
            get { return j2_left1; }
        }
        public String j2_Left2
        {
            set
            {
                if (j2_left2 != value)
                {
                    j2_left2 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j2_Left2"));
                    }
                }
            }
            get { return j2_left2; }
        }
        public String j2_Left3
        {
            set
            {
                if (j2_left3 != value)
                {
                    j2_left3 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j2_Left3"));
                    }
                }
            }
            get { return j2_left3; }
        }
        public String j2_Left4
        {
            set
            {
                if (j2_left4 != value)
                {
                    j2_left4 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j2_Left4"));
                    }
                }
            }
            get { return j2_left4; }
        }
        public String j2_Left5
        {
            set
            {
                if (j2_left5 != value)
                {
                    j2_left5 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j2_Left5"));
                    }
                }
            }
            get { return j2_left5; }
        }
        public String j2_Centro
        {
            set
            {
                if (j2_centro != value)
                {
                    j2_centro = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j2_Centro"));
                    }
                }
            }
            get { return j2_centro; }
        }
        public String j2_Right1
        {
            set
            {
                if (j2_right1 != value)
                {
                    j2_right1 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j2_Right1"));
                    }
                }
            }
            get { return j2_right1; }
        }
        public String j2_Right2
        {
            set
            {
                if (j2_right2 != value)
                {
                    j2_right2 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j2_Right2"));
                    }
                }
            }
            get { return j2_right2; }
        }
        public String j2_Right3
        {
            set
            {
                if (j2_right3 != value)
                {
                    j2_right3 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j2_Right3"));
                    }
                }
            }
            get { return j2_right3; }
        }
        public String j2_Right4
        {
            set
            {
                if (j2_right4 != value)
                {
                    j2_right4 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j2_Right4"));
                    }
                }
            }
            get { return j2_right4; }
        }
        public String j2_Right5
        {
            set
            {
                if (j2_right5 != value)
                {
                    j2_right5 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j2_Right5"));
                    }
                }
            }
            get { return j2_right5; }
        }
        public String j2_ImagenFondo
        {
            set
            {
                if (j2_imagenFondo != value)
                {
                    j2_imagenFondo = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j2_ImagenFondo"));
                    }
                }
            }
            get { return j2_imagenFondo; }
        }
        public Boolean j2_Left1Enable
        {
            set
            {
                if (j2_left1Enable != value)
                {
                    j2_left1Enable = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j2_Left1Enable"));
                    }
                }
            }
            get { return j2_left1Enable; }
        }
        public Boolean j2_Left2Enable
        {
            set
            {
                if (j2_left2Enable != value)
                {
                    j2_left2Enable = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j2_Left2Enable"));
                    }
                }
            }
            get { return j2_left2Enable; }
        }
        public Boolean j2_Left3Enable
        {
            set
            {
                if (j2_left3Enable != value)
                {
                    j2_left3Enable = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j2_Left3Enable"));
                    }
                }
            }
            get { return j2_left3Enable; }
        }
        public Boolean j2_Left4Enable
        {
            set
            {
                if (j2_left4Enable != value)
                {
                    j2_left4Enable = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j2_Left4Enable"));
                    }
                }
            }
            get { return j2_left4Enable; }
        }
        public Boolean j2_Left5Enable
        {
            set
            {
                if (j2_left5Enable != value)
                {
                    j2_left5Enable = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j2_Left5Enable"));
                    }
                }
            }
            get { return j2_left5Enable; }
        }
        public Boolean j2_CentroEnable
        {
            set
            {
                if (j2_centroEnable != value)
                {
                    j2_centroEnable = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j2_CentroEnable"));
                    }
                }
            }
            get { return j2_centroEnable; }
        }
        public Boolean j2_Right1Enable
        {
            set
            {
                if (j2_right1Enable != value)
                {
                    j2_right1Enable = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j2_Right1Enable"));
                    }
                }
            }
            get { return j2_right1Enable; }
        }
        public Boolean j2_Right2Enable
        {
            set
            {
                if (j2_right2Enable != value)
                {
                    j2_right2Enable = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j2_Right2Enable"));
                    }
                }
            }
            get { return j2_right2Enable; }
        }
        public Boolean j2_Right3Enable
        {
            set
            {
                if (j2_right3Enable != value)
                {
                    j2_right3Enable = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j2_Right3Enable"));
                    }
                }
            }
            get { return j2_right3Enable; }
        }
        public Boolean j2_Right4Enable
        {
            set
            {
                if (j2_right4Enable != value)
                {
                    j2_right4Enable = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j2_Right4Enable"));
                    }
                }
            }
            get { return j2_right4Enable; }
        }
        public Boolean j2_Right5Enable
        {
            set
            {
                if (j2_right5Enable != value)
                {
                    j2_right5Enable = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j2_Right5Enable"));
                    }
                }
            }
            get { return j2_right5Enable; }
        }
        public Boolean j2_VisibleReset
        {
            set
            {
                if (j2_visibleReset != value)
                {
                    j2_visibleReset = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j2_VisibleReset"));
                    }
                }
            }
            get { return j2_visibleReset; }
        }
        //----------- JUEGO 3 -----------//
        public int j3_tiempo
        {
            set
            {
                if (j3_Tiempo != value) { j3_Tiempo = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("j3_tiempo")); } }
            }
            get { return j3_Tiempo; }
        }
        public String j3_TextoJuego
        {
            set
            {
                if (j3_textoJuego != value) { j3_textoJuego = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("j3_TextoJuego")); } }
            }
            get { return j3_textoJuego; }
        }
        public String j3_Paleta1
        {
            set
            {
                if (paleta1 != value) { paleta1 = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("j3_Paleta1")); } }
            }
            get { return paleta1; }
        }
        public String j3_Paleta2
        {
            set
            {
                if (paleta2 != value) { paleta2 = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("j3_Paleta2")); } }
            }
            get { return paleta2; }
        }
        public String j3_Paleta3
        {
            set
            {
                if (paleta3 != value) { paleta3 = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("j3_Paleta3")); } }
            }
            get { return paleta3; }
        }
        public String j3_Bolsa1
        {
            set
            {
                if (j3_bolsa1 != value) { j3_bolsa1 = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("j3_Bolsa1")); } }
            }
            get { return j3_bolsa1; }
        }
        public String j3_Bolsa2
        {
            set
            {
                if (j3_bolsa2 != value) { j3_bolsa2 = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("j3_Bolsa2")); } }
            }
            get { return j3_bolsa2; }
        }
        public String j3_Bolsa3
        {
            set
            {
                if (j3_bolsa3 != value) { j3_bolsa3 = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("j3_Bolsa3")); } }
            }
            get { return j3_bolsa3; }

        }
        public String j3_VisPaleta2
        {
            set
            {
                if (j3_visPaleta2 != value) { j3_visPaleta2 = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("j3_VisPaleta2")); } }
            }
            get { return j3_visPaleta2; }
        }
        public String j3_VisPaleta1
        {
            set
            {
                if (j3_visPaleta1 != value) { j3_visPaleta2 = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("j3_VisPaleta1")); } }
            }
            get { return j3_visPaleta1; }
        }
        public String j3_VisPaleta3
        {
            set
            {
                if (j3_visPaleta3 != value) { j3_visPaleta2 = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("j3_VisPaleta3")); } }
            }
            get { return j3_visPaleta3; }
        }
        public String j3_BEnabled1
        {
            set
            {
                if (j3_bEnabled1 != value) { j3_bEnabled1 = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("j3_BEnabled1")); } }
            }
            get { return j3_bEnabled1; }

        }
        public String j3_BEnabled2
        {
            set
            {
                if (j3_bEnabled2 != value) { j3_bEnabled2 = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("j3_BEnabled2")); } }
            }
            get { return j3_bEnabled2; }

        }
        public String j3_BEnabled3
        {
            set
            {
                if (j3_bEnabled3 != value) { j3_bEnabled3 = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("j3_BEnabled3")); } }
            }
            get { return j3_bEnabled3; }

        }
        //----------- JUEGO 4 -----------//
        public int j4_X
        {
            set
            {
                if (j4_x != value)
                {
                    j4_x = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j4_X"));
                    }
                }
            }
            get { return j4_x; }
        }
        public int j4_Y
        {
            set
            {
                if (j4_y != value)
                {
                    j4_y = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j4_Y"));
                    }
                }
            }
            get { return j4_y; }
        }
        public String j4_Timer
        {
            set
            {
                if (j4_timer != value)
                {
                    j4_timer = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j4_Timer"));
                    }
                }
            }
            get { return j4_timer; }
        }
        public String j4_Puntuacion
        {
            set
            {
                if (j4_puntuacion != value)
                {
                    j4_puntuacion = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j4_Puntuacion"));
                    }
                }
            }
            get { return j4_puntuacion; }
        }
        public Boolean j4_Go
        {
            set
            {
                if (j4_go != value)
                {

                    j4_go = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j4_Go"));
                    }
                }
            }
            get { return j4_go; }
        }
        //----------- JUEGO 5 -----------//
        public int j5_Contador_afir
        {
            set
            {
                if (j5_contador_afir != value)
                {
                    j5_contador_afir = value;
                    if (PropertyChanged != null)

                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j5_Contador_afir"));
                    }
                }
            }
            get { return j5_contador_afir; }
        }
        public int j5_Puntos
        {
            set
            {
                if (j5_puntos != value)
                {
                    j5_puntos = value; if (PropertyChanged != null)
                    { PropertyChanged(this, new PropertyChangedEventArgs("j5_Puntos")); }
                }
            }
            get { return j5_puntos; }
        }
        public int j5_Si
        {
            set
            {
                if (j5_si != value)
                {
                    j5_si = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j5_Si"));
                    }
                }
            }
            get { return j5_si; }
        }
        public int j5_No
        {
            set
            {
                if (j5_no != value)
                {
                    j5_no = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j5_No"));
                    }
                }
            }
            get { return j5_no; }
        }
        public String j5_Color1
        {
            set
            {
                if (j5_color1 != value)
                {
                    j5_color1 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j5_Color1"));

                    }
                }
            }
            get { return j5_color1; }
        }
        public String j5_Text_color
        {
            set
            {
                if (j5_text_color != value)
                {
                    j5_text_color = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j5_Text_color"));
                    }
                }
            }
            get { return j5_text_color; }
        }
        public String j5_BotoSi
        {
            set
            {
                if (j5_botoSi != value)
                {
                    j5_botoSi = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j5_BotoSi"));
                    }
                }
            }
            get { return j5_botoSi; }
        }
        public String j5_BotoNo
        {
            set
            {
                if (j5_botoNo != value)
                {
                    j5_botoNo = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j5_BotoNo"));
                    }
                }
            }
            get { return j5_botoNo; }
        }
        //----------- JUEGO 6 -----------//
        public int j6_X
        {
            set
            {
                if (j6_x != value)
                {
                    j6_x = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j6_X"));
                    }
                }
            }
            get { return j6_x; }
        }
        public int j6_Y
        {
            set
            {
                if (j6_y != value)
                {
                    j6_y = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j6_Y"));
                    }
                }
            }
            get { return j6_y; }
        }
        public int j6_Xp
        {
            set
            {
                if (j6_xp != value)
                {
                    j6_xp = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j6_Xp"));
                    }
                }
            }
            get { return j6_xp; }
        }
        public int j6_Yp
        {
            set
            {
                if (j6_yp != value)
                {
                    j6_yp = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j6_Yp"));
                    }
                }
            }
            get { return j6_yp; }
        }
        public String j6_Timer
        {
            set
            {
                if (j6_timer != value)
                {
                    j6_timer = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j6_Timer"));
                    }
                }
            }
            get { return j6_timer; }
        }
        //----------- JUEGO 7 -----------//
        public int j7_Tiempo
        {
            set
            {
                if (j7_tiempo != value)
                {
                    j7_tiempo = value; 
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j7_Tiempo"));
                    }
                }
            }
            get { return j7_tiempo; }
        }
        public int j7_Puntuacio
        {
            set
            {
                if (j7_puntuacio != value)
                {
                    j7_puntuacio = value; 
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j7_Puntuacio"));
                    }
                }
            }
            get { return j7_puntuacio; }
        }
        public String j7_ImatgeCarta1
        {
            set
            {
                if (j7_imatgeCarta1 != value)
                {
                    j7_imatgeCarta1 = value; 
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j7_ImatgeCarta1"));
                    }
                }
            }
            get { return j7_imatgeCarta1; }
        }
        public String j7_ImatgeCarta2
        {
            set
            {
                if (j7_imatgeCarta2 != value)
                {
                    j7_imatgeCarta2 = value; 
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j7_ImatgeCarta2"));
                    }
                }
            }
            get { return j7_imatgeCarta2; }
        }
        public String j7_ImatgeCarta3
        {
            set
            {
                if (j7_imatgeCarta3 != value)
                {
                    j7_imatgeCarta3 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j7_ImatgeCarta3"));
                    }
                }
            }
            get { return j7_imatgeCarta3; }
        }
        public String j7_ImatgeCarta4
        {
            set
            {
                if (j7_imatgeCarta4 != value)
                {
                    j7_imatgeCarta4 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j7_ImatgeCarta4"));
                    }
                }
            }
            get { return j7_imatgeCarta4; }
        }
        public String j7_ImatgeCarta5
        {
            set
            {
                if (j7_imatgeCarta5 != value)
                {
                    j7_imatgeCarta5 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j7_ImatgeCarta5"));
                    }
                }
            }
            get { return j7_imatgeCarta5; }
        }
        public String j7_ImatgeCarta6
        {
            set
            {
                if (j7_imatgeCarta6 != value)
                {
                    j7_imatgeCarta6 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j7_ImatgeCarta6"));
                    }
                }
            }
            get { return j7_imatgeCarta6; }
        }
        public String j7_ImatgeCarta7
        {
            set
            {
                if (j7_imatgeCarta7 != value)
                {
                    j7_imatgeCarta7 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j7_ImatgeCarta7"));
                    }
                }
            }
            get { return j7_imatgeCarta7; }
        }
        public String j7_ImatgeCarta8
        {
            set
            {
                if (j7_imatgeCarta8 != value)
                {
                    j7_imatgeCarta8 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j7_ImatgeCarta8"));
                    }
                }
            }
            get { return j7_imatgeCarta8; }
        }
        public String j7_ImatgeCarta9
        {
            set
            {
                if (j7_imatgeCarta9 != value)
                {
                    j7_imatgeCarta9 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j7_ImatgeCarta9"));
                    }
                }
            }
            get { return j7_imatgeCarta9; }
        }
        public String j7_ImatgeCarta10
        {
            set
            {
                if (j7_imatgeCarta10 != value)
                {
                    j7_imatgeCarta10 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j7_ImatgeCarta10"));
                    }
                }
            }
            get { return j7_imatgeCarta10; }
        }
        public String j7_ImatgeCarta11
        {
            set
            {
                if (j7_imatgeCarta11 != value)
                {
                    j7_imatgeCarta11 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j7_ImatgeCarta11"));
                    }
                }
            }
            get { return j7_imatgeCarta11; }
        }
        public String j7_ImatgeCarta12
        {
            set
            {
                if (j7_imatgeCarta12 != value)
                {
                    j7_imatgeCarta12 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j7_ImatgeCarta12"));
                    }
                }
            }
            get { return j7_imatgeCarta12; }
        }
        public String j7_TextMostrar
        {
            set
            {
                if (j7_textMostrar != value)
                {
                    j7_textMostrar = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j7_TextMostrar"));
                    }
                }
            }
            get { return j7_textMostrar; }
        }
        public Boolean j7_ActivarCarta1
        {
            set
            {
                if (j7_activarCarta1 != value)
                {
                    j7_activarCarta1 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j7_ActivarCarta1"));
                    }
                }
            }
            get { return j7_activarCarta1; }
        }
        public Boolean j7_ActivarCarta2
        {
            set
            {
                if (j7_activarCarta2 != value)
                {
                    j7_activarCarta2 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j7_ActivarCarta2"));
                    }
                }
            }
            get { return j7_activarCarta2; }
        }
        public Boolean j7_ActivarCarta3
        {
            set
            {
                if (j7_activarCarta3 != value)
                {
                    j7_activarCarta3 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j7_ActivarCarta3"));
                    }
                }
            }
            get { return j7_activarCarta3; }
        }
        public Boolean j7_ActivarCarta4
        {
            set
            {
                if (j7_activarCarta4 != value)
                {
                    j7_activarCarta4 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j7_ActivarCarta4"));
                    }
                }
            }
            get { return j7_activarCarta4; }
        }
        public Boolean j7_ActivarCarta5
        {
            set
            {
                if (j7_activarCarta5 != value)
                {
                    j7_activarCarta5 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j7_ActivarCarta5"));
                    }
                }
            }
            get { return j7_activarCarta5; }
        }
        public Boolean j7_ActivarCarta6
        {
            set
            {
                if (j7_activarCarta6 != value)
                {
                    j7_activarCarta6 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j7_ActivarCarta6"));
                    }
                }
            }
            get { return j7_activarCarta6; }
        }
        public Boolean j7_ActivarCarta7
        {
            set
            {
                if (j7_activarCarta7 != value)
                {
                    j7_activarCarta7 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j7_ActivarCarta7"));
                    }
                }
            }
            get { return j7_activarCarta7; }
        }
        public Boolean j7_ActivarCarta8
        {
            set
            {
                if (j7_activarCarta8 != value)
                {
                    j7_activarCarta8 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j7_ActivarCarta8"));
                    }
                }
            }
            get { return j7_activarCarta8; }
        }
        public Boolean j7_ActivarCarta9
        {
            set
            {
                if (j7_activarCarta9 != value)
                {
                    j7_activarCarta9 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j7_ActivarCarta9"));
                    }
                }
            }
            get { return j7_activarCarta9; }
        }
        public Boolean j7_ActivarCarta10
        {
            set
            {
                if (j7_activarCarta10 != value)
                {
                    j7_activarCarta10 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j7_ActivarCarta10"));
                    }
                }
            }
            get { return j7_activarCarta10; }
        }
        public Boolean j7_ActivarCarta11
        {
            set
            {
                if (j7_activarCarta11 != value)
                {
                    j7_activarCarta11 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j7_ActivarCarta11"));
                    }
                }
            }
            get { return j7_activarCarta11; }
        }
        public Boolean j7_ActivarCarta12
        {
            set
            {
                if (j7_activarCarta12 != value)
                {
                    j7_activarCarta12 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j7_ActivarCarta12"));
                    }
                }
            }
            get { return j7_activarCarta12; }
        }
        public Boolean j7_CartaVisible1
        {
            set
            {
                if (j7_cartaVisible1 != value)
                {
                    j7_cartaVisible1 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j7_CartaVisible1"));
                    }
                }
            }
            get { return j7_cartaVisible1; }
        }
        public Boolean j7_CartaVisible2
        {
            set
            {
                if (j7_cartaVisible2 != value)
                {
                    j7_cartaVisible2 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j7_CartaVisible2"));
                    }
                }
            }
            get { return j7_cartaVisible2; }
        }
        public Boolean j7_CartaVisible3
        {
            set
            {
                if (j7_cartaVisible3 != value)
                {
                    j7_cartaVisible3 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j7_CartaVisible3"));
                    }
                }
            }
            get { return j7_cartaVisible3; }
        }
        public Boolean j7_CartaVisible4
        {
            set
            {
                if (j7_cartaVisible4 != value)
                {
                    j7_cartaVisible4 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j7_CartaVisible4"));
                    }
                }
            }
            get { return j7_cartaVisible4; }
        }
        public Boolean j7_VisibleReiniciar
        {
            set
            {
                if (j7_visibleReiniciar != value)
                {
                    j7_visibleReiniciar = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("j7_VisibleReiniciar"));
                    }
                }
            }
            get { return j7_visibleReiniciar; }
        }
        //----------- RANKING -----------//
        public int jr_Puntos_ranking
        {
            set
            {
                if (jr_puntos_ranking != value)
                {
                    jr_puntos_ranking = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("jr_Puntos_ranking"));
                    }
                }
            }
            get
            {
                return jr_puntos_ranking;
            }
        }
        public String jr_Nombre
        {
            set
            {
                if (jr_nombre != value)
                {
                    jr_nombre = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("jr_Nombre"));
                    }
                }
            }
            get
            {
                return jr_nombre;
            }
        }
        public Boolean jr_VRank
        {
            set
            {
                if (jr_vrank != value)
                {
                    jr_vrank = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("jr_VRank"));
                    }
                }
            }
            get
            {
                return jr_vrank;
            }
        }
        public Boolean jr_VMenu
        {
            set
            {
                if (jr_vmenu != value)
                {
                    jr_vmenu = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("jr_VMenu"));
                    }
                }
            }
            get
            {
                return jr_vmenu;
            }
        }
        public Boolean PasarJuego1
        {
            set
            {
                if (pasarJuego1 != value)
                {
                    pasarJuego1 = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("PasarJuego1"));
                    }
                }
            }
            get
            {
                return pasarJuego1;
            }
        }
        public Boolean PasarJuego2
        {
            set
            {
                if (pasarJuego2 != value)
                {
                    pasarJuego2 = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("PasarJuego2"));
                    }
                }
            }
            get
            {
                return pasarJuego2;
            }
        }
        public Boolean PasarJuego3
        {
            set
            {
                if (pasarJuego3 != value)
                {
                    pasarJuego3 = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("PasarJuego3"));
                    }
                }
            }
            get
            {
                return pasarJuego3;
            }
        }
        public Boolean PasarJuego4
        {
            set
            {
                if (pasarJuego4 != value)
                {
                    pasarJuego4 = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("PasarJuego4"));
                    }
                }
            }
            get
            {
                return pasarJuego4;
            }
        }
        public Boolean PasarJuego5
        {
            set
            {
                if (pasarJuego5 != value)
                {
                    pasarJuego5 = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("PasarJuego5"));
                    }
                }
            }
            get
            {
                return pasarJuego5;
            }
        }
        public Boolean PasarJuego6
        {
            set
            {
                if (pasarJuego6 != value)
                {
                    pasarJuego6 = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("PasarJuego6"));
                    }
                }
            }
            get
            {
                return pasarJuego6;
            }
        }
        public Boolean PasarJuego7
        {
            set
            {
                if (pasarJuego7 != value)
                {
                    pasarJuego7 = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("PasarJuego7"));
                    }
                }
            }
            get
            {
                return pasarJuego7;
            }
        }
        public Boolean PasarJuego8
        {
            set
            {
                if (pasarJuego8 != value)
                {
                    pasarJuego8 = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("PasarJuego8"));
                    }
                }
            }
            get
            {
                return pasarJuego8;
            }
        }

        public MainPageViewModel(ContentPage navi)
        {
            nav = navi;
            j1_ComandoGo = new Command(j1_Go);
            j1_CespedC = new Command(j1_Cesped);
            j1_OvejaC = new Command(j1_Oveja);
            j1_LoboC = new Command(j1_LoboAlpha);
            j2_BotoLeft1 = new Command(j2_BotonLeft1);
            j2_BotoLeft2 = new Command(j2_BotonLeft2);
            j2_BotoLeft3 = new Command(j2_BotonLeft3);
            j2_BotoLeft4 = new Command(j2_BotonLeft4);
            j2_BotoLeft5 = new Command(j2_BotonLeft5);
            j2_BotoCentro = new Command(j2_BotonCentro);
            j2_BotoRight1 = new Command(j2_BotonRight1);
            j2_BotoRight2 = new Command(j2_BotonRight2);
            j2_BotoRight3 = new Command(j2_BotonRight3);
            j2_BotoRight4 = new Command(j2_BotonRight4);
            j2_BotoRight5 = new Command(j2_BotonRight5);
            j2_BotoReset = new Command(j2_ResetAll);
            j3_CBolsa1 = new Command(j3_MetodoBolsa1);
            j3_CBolsa2 = new Command(j3_MetodoBolsa2);
            j3_CBolsa3 = new Command(j3_MetodoBolsa3);
            j3_CPaleta1 = new Command(j3_MetodoPaletas1);
            j3_CPaleta2 = new Command(j3_MetodoPaletas2);
            j3_CPaleta3 = new Command(j3_MetodoPaletas3);
            j5_ComandoSi = new Command(j5_Afirmativo);
            j5_ComandoNo = new Command(j5_Negativo);
            j7_BotoCarta1 = new Command(j7_Carta1);
            j7_BotoCarta2 = new Command(j7_Carta2);
            j7_BotoCarta3 = new Command(j7_Carta3);
            j7_BotoCarta4 = new Command(j7_Carta4);
            j7_BotoCarta5 = new Command(j7_Carta5);
            j7_BotoCarta6 = new Command(j7_Carta6);
            j7_BotoCarta7 = new Command(j7_Carta7);
            j7_BotoCarta8 = new Command(j7_Carta8);
            j7_BotoCarta9 = new Command(j7_Carta9);
            j7_BotoCarta10 = new Command(j7_Carta10);
            j7_BotoCarta11 = new Command(j7_Carta11);
            j7_BotoCarta12 = new Command(j7_Carta12);
            j7_BotoReiniciar = new Command(j7_Reiniciar);
            jr_cNom = new Command(Basededatos);

            j2_ImagenFondo = "j2_fondo1.png";
            j5_BotoSi = "j5_verde1.png";
            j5_BotoNo = "j5_rojo1.png";
            j7_ArrayFotos[0] = "j7_barril.png";
            j7_ArrayFotos[1] = "j7_burger.png";
            j7_ArrayFotos[2] = "j7_campana.png";
            j7_ArrayFotos[3] = "j7_corazon.png";
            j7_ArrayFotos[4] = "j7_diamante.png";
            j7_ArrayFotos[5] = "j7_dinero.png";
            j7_ArrayFotos[6] = "j7_moneda.png";
            j7_ArrayFotos[7] = "j7_quadradoamarillo.png";
            j7_ArrayFotos[8] = "j7_quadradogris.png";
            j7_ArrayFotos[9] = "j7_quadradorojo.png";
            j7_ArrayFotos[10] = "j7_quadradoverde.png";
            j7_ArrayFotos[11] = "j7_reloj.png";
            j7_ArrayFotos[12] = "j7_cireres.png";
            j7_ArrayFotos[13] = "j7_fresa.png";
            j7_ArrayFotos[14] = "j7_limon.png";
            j7_ArrayFotos[15] = "j7_manzana.png";
            j7_ArrayFotos[16] = "j7_naranja.png";
            j7_ArrayFotos[17] = "j7_sandia.png";
            j7_ArrayFotos[18] = "j7_uvas.png";

            j1_Comprobaciones();
            //j1_Tiempo2();
            j2_FuncionUnaVez();
            //j2_IniciarCronometro();
            j4_PosiRandom();
            //j4_Reloj();
            j4_Puntuador();
            j4_Stop();
            j5_Colores();
            //j6_Reloj();
            j7_RandomCartes();
            //j7_Temps();

        }

        //----------------------------------- FUNCIONES -----------------------------------//
        //----------- JUEGO 1 -----------//
        public void j1_LoboAlpha()
        {
            if (j1_tiempobool == false)
            {
                j1_Tiempo2();
                j1_tiempobool = true;
            }
            if (j1_BarcoUnlock == true && j1_Chivato_botones == true && j1_Warning == false)  // en caso de que chivato botones sea true y barcounlock sea true y warning sea false
            {
                if (j1_Barcox == 100 && j1_lobopos == false)  // entra en otro if  si el barco esta a la derecha con las coordedenadas X (100) y la posicion del lobo es false (false == derecha , true == izquierda)
                {
                    j1_lobolock = true;  // bloqueo el lobo para que no pueda clickarlo mas
                    j1_lobo++;  // le sumo 1 punto al lobo para que del 0 pase a 1 
                    j1_Lobox = j1_Barcox; // Subo el lobo al barco para que luego la persona o el usuario le de al go
                    j1_Chivato_botones = false;  // chivato de botones lo pongo en false para que no sele pueda dar mas al lobo
                }

                if (j1_Barcox == -120 && j1_lobopos == true)   // lo mismo pero si el lobo esta en la parte izquierda y el barco este en la parte izquierda
                {
                    j1_lobolock = true;
                    j1_lobo++;
                    j1_Lobox = j1_Barcox;
                    j1_Chivato_botones = false;
                }
            }
        }
        public void j1_Oveja()
        {
            if (j1_tiempobool == false)
            {
                j1_Tiempo2();
                j1_tiempobool = true;
            }
            if (j1_BarcoUnlock == true && j1_Chivato_botones == true && j1_Warning == false)
            {
                if (j1_Barcox == 100 && j1_ovejapos == false)
                {
                    j1_ovejalock = true;
                    j1_oveja++;
                    j1_Ovejax = j1_Barcox;
                    j1_Chivato_botones = false;
                }

                if (j1_Barcox == -120 && j1_ovejapos == true)
                {
                    j1_ovejalock = true;
                    j1_oveja++;
                    j1_Ovejax = j1_Barcox;
                    j1_Chivato_botones = false;
                }
            }
        }
        public void j1_Cesped()
        {
            if (j1_tiempobool == false)
            {
                j1_Tiempo2();
                j1_tiempobool = true;
            }
            if (j1_BarcoUnlock == true && j1_Chivato_botones == true && j1_Warning == false)
            {
                if (j1_Barcox == 100 && j1_cespedpos == false)
                {
                    j1_cespedlock = true;
                    j1_cesped++;
                    j1_Cespedx = j1_Barcox;
                    j1_Chivato_botones = false;


                }

                if (j1_Barcox == -120 && j1_cespedpos == true)
                {
                    j1_cespedlock = true;
                    j1_cesped++;
                    j1_Cespedx = j1_Barcox;
                    j1_Chivato_botones = false;



                }
            }
        }
        public void j1_Go()
        {
            if (j1_tiempobool == false)
            {
                j1_Tiempo2();
                j1_tiempobool = true;
            }
            if (j1_BarcoUnlock == true && j1_Warning == false)
            {
                j1_Barcodir++;         // Barco direccion +1, para que en funcion de que numero es en el hilo se va a comportar de una forma u otra
                j1_posB = true;        // Activo el hilo principal 
                j1_Desplazamientos();  // Llamo al hilo principal

            }
        }
        async Task j1_Comprobaciones()
        {
            while (true)
            {
                //Direccion del barco hacia izquierda

                if (j1_lobopos == false && j1_ovejapos == false && j1_cespedpos == false && j1_Barcox == -120)  // en caso de que las posiciones de todos sea = false y el barco este en -120 que estaria en la izquierda y los otros objetos en la derecha 
                {
                    if (j1_Warning == false)
                    {
                        j1_Warning = true;
                        await Application.Current.MainPage.DisplayAlert("Has Perdido", "El lobo se ha comido la oveja", "Cerrar");
                        j1_Tiempo = 0;
                        PasarJuego1 = true;
                    }
                    j1_posB = false;               // Paro el hilo principal
                    j1_Chivato_botones = false;    // Bloqueo de todos los botones
                }

                if (j1_lobopos == false && j1_ovejapos == false && j1_cespedpos == true && j1_Barcox == -120)  // si la posicion del lobo es false (derecha) y oveja es false (derecha) y el cesped es true (izquierda)
                {
                    if (j1_Warning == false)
                    {
                        j1_Warning = true;
                        await Application.Current.MainPage.DisplayAlert("Has Perdido", "El lobo se ha comido la oveja", "Cerrar");
                        j1_Tiempo = 0;
                        PasarJuego1 = true;
                    }
                    j1_posB = false;
                    j1_Chivato_botones = false;
                }

                if (j1_lobopos == true && j1_ovejapos == false && j1_cespedpos == false && j1_Barcox == -120)   // en caso de que me haya llevado el lobo a la izquierda y el barco esta en la parte izquierda de la orilla y el cesped y oveja esta en la parte derecha
                {
                    if (j1_Warning == false)
                    {
                        j1_Warning = true;
                        await Application.Current.MainPage.DisplayAlert("Has Perdido", "La oveja se ha comdido el cesped", "Cerrar");
                        j1_Tiempo = 0;
                        PasarJuego1 = true;
                    }
                    j1_posB = false;
                    j1_Chivato_botones = false;
                }

                if (j1_lobopos == true && j1_ovejapos == true && j1_cespedpos == true && j1_Barcox == -120)   // en caso de que todos estan en la otra parte de la orilla en la parte izquierda 
                {
                    if (j1_Warning == false)
                    {
                        j1_puntos2 = j1_Puntos;
                        j1_Warning = true;
                        await Application.Current.MainPage.DisplayAlert("Has Ganado", "Has conseguido " + j1_Puntos.ToString() + " Puntos", "Cerrar");
                        j1_Tiempo = 0;
                        j1_Puntos = j1_puntos2;
                        PasarJuego1 = true;
                    }
                    j1_ganada = true;
                    j1_posB = false;
                    j1_Chivato_botones = false;
                }

                //Direccion del barco hacia derecha
                if (j1_lobopos == true && j1_ovejapos == true && j1_cespedpos == false && j1_Barcox == 100) // mas de lo mismo perro en en el barco hacia derecha si la posicion del lobo es true y oveja es true y cesped es false y el barco esta a +100 de la coordenadas X  salta el siguiente error
                {
                    if (j1_Warning == false)
                    {
                        j1_Warning = true;
                        await Application.Current.MainPage.DisplayAlert("Has Perdido", "El lobo se ha comido la oveja", "Cerrar");
                        j1_Tiempo = 0;
                        PasarJuego1 = true;
                    }
                    j1_posB = false;
                    j1_Chivato_botones = false;
                }

                if (j1_lobopos == false && j1_ovejapos == true && j1_cespedpos == true && j1_Barcox == 100)  // en caso de que la posicion del lobo es false (derecha) y la oveja esta en true (izquierda) y el cesped esta en true (izquierda)  y las coordenadas del barco esta en +100  (derecha)
                {
                    if (j1_Warning == false)
                    {
                        j1_Warning = true;
                        await Application.Current.MainPage.DisplayAlert("Has Perdido", "La oveja se ha comdido el cesped", "Cerrar");
                        j1_Tiempo = 0;
                        PasarJuego1 = true;
                    }
                    j1_posB = false;
                    j1_Chivato_botones = false;
                }

                if (j1_Tiempo <= 1 && j1_ganada == false)
                {
                    if (j1_Warning == false)
                    {
                        j1_Warning = true;
                        await Application.Current.MainPage.DisplayAlert("Has Perdido", "Se ha acabado el Tiempo", "Cerrar");
                        j1_Tiempo = 0;
                        PasarJuego1 = true;
                    }
                    j1_posB = false;
                    j1_Chivato_botones = false;
                }

                await Task.Delay(1);
            }
        }
        async Task j1_Tiempo2()
        {
            j1_Puntos = 20000;
            while (j1_Tiempo > 0 && j1_Warning == false)
            {  // en caso de que el tiempo sea mayor a 0 y la variable warning que te avisa si se acabo el juego

                await Task.Delay(1000);

                j1_Tiempo--;       // La variable del tiempo que esta inicializada a 45, por cada segundo le va a bajar un 1 que se traduce a bajar el tiempo en 1
                j1_Puntos -= 500;   // por cada segundo le baja 50 puntos de 1750 para saber cuantos puntos va a sacar de este minijuego

                if (j1_Warning == true && j1_ganada == false)
                {
                    // En caso de que el booleano ganada sea false, no obtiene puntos y despues deberia pasar al siguiente juego
                 
                    j1_Puntos = 0;
                }

            }

            if (j1_ganada == true && j1_Warning == true)
            {
                if (j1_Tiempo >  15)
                {
                    j1_Puntos = 20000;
                    await Application.Current.MainPage.DisplayAlert("Has ganado 20000", "Por terminar el juego en menos de 35 segundos", "Cerrar");
                    
                }
                PasarJuego1 = true;
                    // Pasar al siguiente juego
                
            }

        }
        async Task j1_Desplazamientos()
        {
            while (j1_posB == true)        // posB la variable 
            {
                await Task.Delay(1); // el tiempo para hacer todo el bucle

                if (j1_Barcodir > 2)   // en caso de que la direccion del barco sea mayor a 2 lo pongo en 0
                {
                    j1_Barcodir = 0;
                }
                if (j1_cesped > 2)  // lo mismo para el cesped  y la oveja y el lobo que esta unas lineas mas
                {
                    j1_cesped = 0;
                }
                if (j1_oveja > 2)
                {
                    j1_oveja = 0;
                }
                if (j1_lobo > 2)
                {
                    j1_lobo = 0;
                }

                if (j1_goaa == true)  // el barco ira hacia izquierda parte del hilo
                {

                    if (j1_Barcodir == 1)          // barco direccion 1 es que ira hacia la izquierda
                    {
                        j1_Barcox -= 5;               // restando cooredenadas para que vaya hacia izquierda
                        j1_BarcoUnlock = false;   // bloqueando el barco para que alguien no pueda buggear el juego y mover el barco donde el quiera

                        if (j1_Barcodir == 1 && (j1_oveja == 1 || j1_oveja == 0))   //ira hacia izquierda
                        {

                            if (j1_oveja == 1 && j1_ovejalock == true)  // en caso de que la haya dado a la oveja y el bloqueo es igual a true osea que puede darle
                            {
                                j1_Ovejax = j1_Barcox;            // las coordenadas del barco se igualan a la oveja
                                if (j1_Barcox == -120)     // en caso de que las coordenadas del barco sea igual  a -100
                                {
                                    j1_Ovejax = -275;      // pone la oveja a la otra parte de la orilla
                                    j1_ovejalock = false;  // le desbloquea el boton para que pueda volver a pulsarlo
                                    j1_ovejapos = true;    // variable para saber la posicion de la oveja si es true esta a la izquierda y si es falso esta a la derecha
                                }
                            }
                        }

                        if (j1_Barcodir == 1 && (j1_cesped == 1 || j1_cesped == 0))   // la misma funcion de la oveja pero para el cesped
                        {
                            if (j1_cesped == 1 && j1_cespedlock == true)
                            {
                                j1_Cespedx = j1_Barcox;
                                if (j1_Barcox == -120)
                                {
                                    j1_Cespedx = -220;
                                    j1_cespedlock = false;
                                    j1_cespedpos = true;
                                }
                            }
                        }

                        if (j1_Barcodir == 1 && (j1_lobo == 1 || j1_lobo == 0))  // la misma funcion de la oveja pero para el lobo
                        {
                            if (j1_lobo == 1 && j1_lobolock == true)
                            {
                                j1_Lobox = j1_Barcox;
                                if (j1_Barcox == -120)
                                {
                                    j1_Lobox = -360;
                                    j1_lobolock = false;
                                    j1_lobopos = true;
                                }
                            }
                        }

                        if (j1_Barcox == -120)
                        {  // si las coordenadas X del barco llegan a ser a -120   

                            j1_BarcoUnlock = true;          // desbloquea el barco para que pueda volver a clickarlo
                            j1_goaa = false;                // para la primera parte del hilo para que el barco no coja y vaya ala izquierda
                            j1_gobb = true;                 // abro la segunda parte del hilo que esta mas para abajo para que el barco luego vaya hacia la derecha
                            j1_posB = false;                // paro el hilo completo hasta que el usuario le da otra vez al Go que es para mover el barcdo
                            j1_Chivato_botones = true;      // el chivato de botones lo uso para que en caso de que le haya dado a un boton esta bindeado con todos los botones y no pueda dar a otro boton en este caso al estar en true puede.
                        }
                    }
                }
                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                if (j1_gobb == true)  // parte del hilo que se ocupara de los objetos para llevarlos a la derecha
                {

                    if (j1_Barcodir == 2) // el barco ira hacia derecha si la direccion del barco es 2
                    {
                        j1_Barcox += 5;               // sumando las coordenadas X al barco
                        j1_BarcoUnlock = false;    // bloquea el boton Go (el barco para que no pueda darle mas )

                        if (j1_Barcodir == 2 && j1_cesped == 2) // lo mismo para la funcion de la obeja mas arriba pero en este caso que vaya hacia derecha
                        {
                            j1_Barcox+=5;
                            j1_BarcoUnlock = false;
                            j1_cespedlock = true;

                            if (j1_cesped == 2 && j1_cespedlock == true) //cesped hacia derecha
                            {
                                j1_Cespedx = j1_Barcox;
                                if (j1_Barcox == 100)
                                {
                                    j1_Cespedx = +210;
                                    j1_cespedlock = false;
                                    j1_cespedpos = false;
                                    j1_cesped = 0;
                                }
                            }
                        }

                        if (j1_Barcodir == 2 && j1_oveja == 2)  //oveja hacia derecha
                        {
                            j1_Barcox+=5;
                            j1_BarcoUnlock = false;
                            j1_ovejalock = true;

                            if (j1_oveja == 2 && j1_ovejalock == true && j1_ovejapos == true)
                            {
                                j1_Ovejax = j1_Barcox;

                                if (j1_Barcox == 100)
                                {
                                    j1_Ovejax = +265;
                                    j1_ovejalock = false;
                                    j1_ovejapos = false;
                                    j1_oveja = 0;
                                }
                            }
                        }

                        if (j1_Barcodir == 2 && j1_lobo == 2)   // lobo hacia derecha 
                        {
                            j1_Barcox+=5;
                            j1_BarcoUnlock = false;

                            if (j1_lobo == 2 && j1_lobolock == true)
                            {
                                j1_Lobox = j1_Barcox;
                                if (j1_Barcox == 100)
                                {
                                    j1_Lobox = 320;
                                    j1_lobolock = false;
                                    j1_lobo = 0;
                                    j1_lobopos = false;
                                }
                            }
                        }

                        if (j1_Barcox == 100)            // en caso de que las coordenadas X del barco sea = 100
                        {
                            j1_posB = false;               // paro hilo principal
                            j1_gobb = false;               // paro la segunda parte del hilo
                            j1_goaa = true;                // enciendo la primera parte del hilo
                            j1_BarcoUnlock = true;         // desbloqueo el barco para que pueda volver a clickarlo
                            j1_Chivato_botones = true;     // desbloqueo los botones para que pueda pulsar otra vez a cualquier boton el usuario
                        }
                    }

                    if (j1_Barcodir > 2)
                    {
                        j1_Barcodir = 1;
                    }
                }

            }   // finish while principal
        }
        //----------- JUEGO 2 -----------//
        public void j2_FuncionUnaVez()
        {

            //Declarar cada imagen en el array
            if (j2_flagbooluna == true)
            {
                j2_ArrayRanas[0] = "j2_blava1.png";
                j2_ArrayRanas[1] = "j2_blava2.png";
                j2_ArrayRanas[2] = "j2_blava3.png";
                j2_ArrayRanas[3] = "j2_blava4.png";
                j2_ArrayRanas[4] = "j2_blava5.png";
                j2_ArrayRanas[5] = "j2_vacio.png";
                j2_ArrayRanas[6] = "j2_verd1.png";
                j2_ArrayRanas[7] = "j2_verd2.png";
                j2_ArrayRanas[8] = "j2_verd3.png";
                j2_ArrayRanas[9] = "j2_verd4.png";
                j2_ArrayRanas[10] = "j2_verd5.png";
            }
            j2_flagbooluna = false;
            j2_VisibleReset = false;
        }
        public void j2_BotonLeft1()
        {
            j2_flagcarta = j2_ArrayRanas[0];
            for (int j2_i = 0; j2_i < j2_ArrayRanas.Length; j2_i++)
            {
                if (j2_ArrayRanas[j2_i] == "j2_vacio.png")
                {
                    j2_z = j2_i;
                    j2_ArrayRanas[j2_i] = j2_flagcarta;
                }
            }
            j2_ArrayRanas[0] = "j2_vacio.png";
            j2_AssignarLlocs();
        }
        public void j2_BotonLeft2()
        {
            j2_flagcarta = j2_ArrayRanas[1];
            for (int j2_i = 0; j2_i < j2_ArrayRanas.Length; j2_i++)
            {
                if (j2_ArrayRanas[j2_i] == "j2_vacio.png")
                {
                    j2_z = j2_i;
                    j2_ArrayRanas[j2_i] = j2_flagcarta;
                }
            }
            j2_ArrayRanas[1] = "j2_vacio.png";
            j2_AssignarLlocs();
        }
        public void j2_BotonLeft3()
        {
            j2_flagcarta = j2_ArrayRanas[2];
            for (int j2_i = 0; j2_i < j2_ArrayRanas.Length; j2_i++)
            {
                if (j2_ArrayRanas[j2_i] == "j2_vacio.png")
                {
                    j2_z = j2_i;
                    j2_ArrayRanas[j2_i] = j2_flagcarta;
                }
            }
            j2_ArrayRanas[2] = "j2_vacio.png";
            j2_AssignarLlocs();
        }
        public void j2_BotonLeft4()
        {
            j2_flagcarta = j2_ArrayRanas[3];
            for (int j2_i = 0; j2_i < j2_ArrayRanas.Length; j2_i++)
            {
                if (j2_ArrayRanas[j2_i] == "j2_vacio.png")
                {
                    j2_z = j2_i;
                    j2_ArrayRanas[j2_i] = j2_flagcarta;
                }
            }
            j2_ArrayRanas[3] = "j2_vacio.png";
            j2_AssignarLlocs();
        }
        public void j2_BotonLeft5()
        {
            j2_flagcarta = j2_ArrayRanas[4];
            for (int j2_i = 0; j2_i < j2_ArrayRanas.Length; j2_i++)
            {
                if (j2_ArrayRanas[j2_i] == "j2_vacio.png")
                {
                    j2_z = j2_i;
                    j2_ArrayRanas[j2_i] = j2_flagcarta;
                }
            }
            j2_ArrayRanas[4] = "j2_vacio.png";
            j2_AssignarLlocs();
        }
        public void j2_BotonCentro()
        {
            j2_flagcarta = j2_ArrayRanas[5];
            for (int j2_i = 0; j2_i < j2_ArrayRanas.Length; j2_i++)
            {
                if (j2_ArrayRanas[j2_i] == "j2_vacio.png")
                {
                    j2_z = j2_i;
                    j2_ArrayRanas[j2_i] = j2_flagcarta;
                }
            }
            j2_ArrayRanas[5] = "j2_vacio.png";
            j2_AssignarLlocs();
        }
        public void j2_BotonRight1()
        {
            j2_flagcarta = j2_ArrayRanas[6];
            for (int j2_i = 0; j2_i < j2_ArrayRanas.Length; j2_i++)
            {
                if (j2_ArrayRanas[j2_i] == "j2_vacio.png")
                {
                    j2_z = j2_i;
                    j2_ArrayRanas[j2_i] = j2_flagcarta;
                }
            }
            j2_ArrayRanas[6] = "j2_vacio.png";
            j2_AssignarLlocs();
        }
        public void j2_BotonRight2()
        {
            j2_flagcarta = j2_ArrayRanas[7];
            for (int j2_i = 0; j2_i < j2_ArrayRanas.Length; j2_i++)
            {
                if (j2_ArrayRanas[j2_i] == "j2_vacio.png")
                {
                    j2_z = j2_i;
                    j2_ArrayRanas[j2_i] = j2_flagcarta;
                }
            }
            j2_ArrayRanas[7] = "j2_vacio.png";
            j2_AssignarLlocs();
        }
        public void j2_BotonRight3()
        {
            j2_flagcarta = j2_ArrayRanas[8];
            for (int j2_i = 0; j2_i < j2_ArrayRanas.Length; j2_i++)
            {
                if (j2_ArrayRanas[j2_i] == "j2_vacio.png")
                {
                    j2_z = j2_i;
                    j2_ArrayRanas[j2_i] = j2_flagcarta;
                }
            }
            j2_ArrayRanas[8] = "j2_vacio.png";
            j2_AssignarLlocs();
        }
        public void j2_BotonRight4()
        {
            j2_flagcarta = j2_ArrayRanas[9];
            for (int j2_i = 0; j2_i < j2_ArrayRanas.Length; j2_i++)
            {
                if (j2_ArrayRanas[j2_i] == "j2_vacio.png")
                {
                    j2_z = j2_i;
                    j2_ArrayRanas[j2_i] = j2_flagcarta;
                }
            }
            j2_ArrayRanas[9] = "j2_vacio.png";
            j2_AssignarLlocs();
        }
        public void j2_BotonRight5()
        {
            j2_flagcarta = j2_ArrayRanas[10];
            for (int j2_i = 0; j2_i < j2_ArrayRanas.Length; j2_i++)
            {
                if (j2_ArrayRanas[j2_i] == "j2_vacio.png")
                {
                    j2_z = j2_i;
                    j2_ArrayRanas[j2_i] = j2_flagcarta;
                }
            }
            j2_ArrayRanas[10] = "j2_vacio.png";
            j2_AssignarLlocs();
        }
        public void j2_EnableLLocs()
        {
            //Declaro cada carta indicando quales se pueden activar a su lado
            for (j2_i = 0; j2_i < j2_ArrayRanasBool.Length; j2_i++)
            {
                if (j2_ArrayRanas[0] == "j2_vacio.png")
                {
                    j2_ArrayRanasBool[j2_i] = false;
                    j2_ArrayRanasBool[0] = false;
                    j2_ArrayRanasBool[0 + 1] = true;
                    j2_ArrayRanasBool[0 + 2] = true;
                }
                else if (j2_ArrayRanas[1] == "j2_vacio.png")
                {
                    j2_ArrayRanasBool[j2_i] = false;
                    j2_ArrayRanasBool[1] = false;
                    j2_ArrayRanasBool[1 - 1] = true;
                    j2_ArrayRanasBool[1 + 1] = true;
                    j2_ArrayRanasBool[1 + 2] = true;
                }
                else if (j2_ArrayRanas[2] == "j2_vacio.png")
                {
                    j2_ArrayRanasBool[j2_i] = false;
                    j2_ArrayRanasBool[2 + 1] = true;
                    j2_ArrayRanasBool[2 + 2] = true;
                    j2_ArrayRanasBool[2 - 1] = true;
                    j2_ArrayRanasBool[2 - 2] = true;
                }
                else if (j2_ArrayRanas[3] == "j2_vacio.png")
                {
                    j2_ArrayRanasBool[j2_i] = false;
                    j2_ArrayRanasBool[3 + 1] = true;
                    j2_ArrayRanasBool[3 + 2] = true;
                    j2_ArrayRanasBool[3 - 1] = true;
                    j2_ArrayRanasBool[3 - 2] = true;
                }
                else if (j2_ArrayRanas[4] == "j2_vacio.png")
                {
                    j2_ArrayRanasBool[j2_i] = false;
                    j2_ArrayRanasBool[4 + 1] = true;
                    j2_ArrayRanasBool[4 + 2] = true;
                    j2_ArrayRanasBool[4 - 1] = true;
                    j2_ArrayRanasBool[4 - 2] = true;
                }
                else if (j2_ArrayRanas[5] == "j2_vacio.png")
                {
                    j2_ArrayRanasBool[j2_i] = false;
                    j2_ArrayRanasBool[5 + 1] = true;
                    j2_ArrayRanasBool[5 + 2] = true;
                    j2_ArrayRanasBool[5 - 1] = true;
                    j2_ArrayRanasBool[5 - 2] = true;
                }
                else if (j2_ArrayRanas[6] == "j2_vacio.png")
                {
                    j2_ArrayRanasBool[j2_i] = false;
                    j2_ArrayRanasBool[6 + 1] = true;
                    j2_ArrayRanasBool[6 + 2] = true;
                    j2_ArrayRanasBool[6 - 1] = true;
                    j2_ArrayRanasBool[6 - 2] = true;
                }
                else if (j2_ArrayRanas[7] == "j2_vacio.png")
                {
                    j2_ArrayRanasBool[j2_i] = false;
                    j2_ArrayRanasBool[7 + 1] = true;
                    j2_ArrayRanasBool[7 + 2] = true;
                    j2_ArrayRanasBool[7 - 1] = true;
                    j2_ArrayRanasBool[7 - 2] = true;
                }
                else if (j2_ArrayRanas[8] == "j2_vacio.png")
                {
                    j2_ArrayRanasBool[j2_i] = false;
                    j2_ArrayRanasBool[8 + 1] = true;
                    j2_ArrayRanasBool[8 + 2] = true;
                    j2_ArrayRanasBool[8 - 1] = true;
                    j2_ArrayRanasBool[8 - 2] = true;
                }
                else if (j2_ArrayRanas[9] == "j2_vacio.png")
                {
                    j2_ArrayRanasBool[j2_i] = false;
                    j2_ArrayRanasBool[9] = false;
                    j2_ArrayRanasBool[9 + 1] = true;
                    j2_ArrayRanasBool[9 - 1] = true;
                    j2_ArrayRanasBool[9 - 2] = true;
                }
                else if (j2_ArrayRanas[10] == "j2_vacio.png")
                {
                    j2_ArrayRanasBool[j2_i] = false;
                    j2_ArrayRanasBool[10] = false;
                    j2_ArrayRanasBool[10 - 1] = true;
                    j2_ArrayRanasBool[10 - 2] = true;
                }
                j2_ArrayRanasBool[j2_z] = false; //Rana actual desactivar
            }
            j2_AssignarEnables(); //Llamo a la funcion que asigna cada array a la posicion
        }
        public void j2_AssignarEnables()
        {
            //Asigno cada booleano a cada rana
            j2_Left1Enable = j2_ArrayRanasBool[0];
            j2_Left2Enable = j2_ArrayRanasBool[1];
            j2_Left3Enable = j2_ArrayRanasBool[2];
            j2_Left4Enable = j2_ArrayRanasBool[3];
            j2_Left5Enable = j2_ArrayRanasBool[4];
            j2_CentroEnable = j2_ArrayRanasBool[5];
            j2_Right1Enable = j2_ArrayRanasBool[6];
            j2_Right2Enable = j2_ArrayRanasBool[7];
            j2_Right3Enable = j2_ArrayRanasBool[8];
            j2_Right4Enable = j2_ArrayRanasBool[9];
            j2_Right5Enable = j2_ArrayRanasBool[10];
            j2_ComprobarBe(); //Funcion que comprueba la posicion de las cartas
        }
        public void j2_AssignarLlocs()
        {
            PasarJuego2 = false;
            if (j2_tiempobool == false)
            {
                j2_IniciarCronometro();
                j2_tiempobool = true;

            }
            //Asignar posiciones
            j2_Left1 = j2_ArrayRanas[0];
            j2_Left2 = j2_ArrayRanas[1];
            j2_Left3 = j2_ArrayRanas[2];
            j2_Left4 = j2_ArrayRanas[3];
            j2_Left5 = j2_ArrayRanas[4];
            j2_Centro = j2_ArrayRanas[5];
            j2_Right1 = j2_ArrayRanas[6];
            j2_Right2 = j2_ArrayRanas[7];
            j2_Right3 = j2_ArrayRanas[8];
            j2_Right4 = j2_ArrayRanas[9];
            j2_Right5 = j2_ArrayRanas[10];
            j2_flagcarta = "";
            j2_NumClicks--; //Restar puntos en cada movimiento
            j2_EnableLLocs();
        }
        public void j2_ComprobarBe()
        {
            if (j2_NumClicks == 0)
            {
                j2_ImagenFondo = "j2_gameover.png";
                j2_Left1Enable = false;
                j2_Left2Enable = false;
                j2_Left3Enable = false;
                j2_Left4Enable = false;
                j2_Left5Enable = false;
                j2_CentroEnable = false;
                j2_Right1Enable = false;
                j2_Right2Enable = false;
                j2_Right3Enable = false;
                j2_Right4Enable = false;
                j2_Right5Enable = false;
                j2_VisibleReset = true;
            }
            //Super if que comprueba todas las posiciones
            else if ((j2_Left1 == "j2_verd1.png" || j2_Left1 == "j2_verd2.png" || j2_Left1 == "j2_verd3.png" || j2_Left1 == "j2_verd4.png" || j2_Left1 == "j2_verd5.png") &&
                (j2_Left2 == "j2_verd1.png" || j2_Left2 == "j2_verd2.png" || j2_Left2 == "j2_verd3.png" || j2_Left2 == "j2_verd4.png" || j2_Left2 == "j2_verd5.png") &&
                (j2_Left3 == "j2_verd1.png" || j2_Left3 == "j2_verd2.png" || j2_Left3 == "j2_verd3.png" || j2_Left3 == "j2_verd4.png" || j2_Left3 == "j2_verd5.png") &&
                (j2_Left4 == "j2_verd1.png" || j2_Left4 == "j2_verd2.png" || j2_Left4 == "j2_verd3.png" || j2_Left4 == "j2_verd4.png" || j2_Left4 == "j2_verd5.png") &&
                (j2_Left5 == "j2_verd1.png" || j2_Left5 == "j2_verd2.png" || j2_Left5 == "j2_verd3.png" || j2_Left5 == "j2_verd4.png" || j2_Left5 == "j2_verd5.png") &&
                (j2_Centro == "j2_vacio.png") &&
                (j2_Right1 == "j2_blava1.png" || j2_Right1 == "j2_blava2.png" || j2_Right1 == "j2_blava3.png" || j2_Right1 == "j2_blava4.png" || j2_Right1 == "j2_blava5.png") &&
                (j2_Right2 == "j2_blava1.png" || j2_Right2 == "j2_blava2.png" || j2_Right2 == "j2_blava3.png" || j2_Right2 == "j2_blava4.png" || j2_Right2 == "j2_blava5.png") &&
                (j2_Right3 == "j2_blava1.png" || j2_Right3 == "j2_blava2.png" || j2_Right3 == "j2_blava3.png" || j2_Right3 == "j2_blava4.png" || j2_Right3 == "j2_blava5.png") &&
                (j2_Right4 == "j2_blava1.png" || j2_Right4 == "j2_blava2.png" || j2_Right4 == "j2_blava3.png" || j2_Right4 == "j2_blava4.png" || j2_Right4 == "j2_blava5.png") &&
                (j2_Right5 == "j2_blava1.png" || j2_Right5 == "j2_blava2.png" || j2_Right5 == "j2_blava3.png" || j2_Right5 == "j2_blava4.png" || j2_Right5 == "j2_blava5.png"))
            {
                //Si entra ganas el juego
                j2_ImagenFondo = "j2_win.png";
                j2_Left1Enable = false;
                j2_Left2Enable = false;
                j2_Left3Enable = false;
                j2_Left4Enable = false;
                j2_Left5Enable = false;
                j2_CentroEnable = false;
                j2_Right1Enable = false;
                j2_Right2Enable = false;
                j2_Right3Enable = false;
                j2_Right4Enable = false;
                j2_Right5Enable = false;
                j2_VisibleReset = true;
                Application.Current.MainPage.DisplayAlert("Felicidades!", "Tu puntuacion es de 20000", "Cerrar");
                j2_Punts = 20000;
                j2_Puntosgl = 0;
                PasarJuego2 = true;
            }

        }
        public void j2_ResetAll()
        {
            //Funcion que resetea todas las variables y arrays
            j2_flagbooluna = true;
            String[] j2_ArrayRanas = new String[11];
            Boolean[] j2_ArrayRanasBool = new Boolean[11];
            j2_FuncionUnaVez();
            j2_BotonCentro();
            j2_ImagenFondo = "fondo1.png";
            j2_Left4Enable = true;
            j2_Left5Enable = true;
            j2_Right1Enable = true;
            j2_Right2Enable = true;
            j2_i = 0;
            j2_z = 99;
            j2_NumClicks = 50;
        }
        public async Task j2_IniciarCronometro()
        {
            do
            {
                j2_Puntosgl--;
                await Task.Delay(1000);

            } while (j2_Puntosgl != 0);

            if (j2_Puntosgl == 0 && j2_Punts!=20000)
            {
                await Application.Current.MainPage.DisplayAlert("GAME OVER", "Tu puntuacion es de " + j2_Punts, "Cerrar");
                j2_Puntosgl = 0;
                PasarJuego2 = true;
            }
        }
        //----------- JUEGO 3 -----------//
        public void j3_MetodoPaletas1()
        {

            if (j3_Clicked == false)
            {

                j3_bolsa1Cliked = true;

                J3_Tiempo2();

            }
            BP1 = true;
        }
        public void j3_MetodoPaletas2()
        {

            if (j3_Clicked == false)
            {

                j3_bolsa1Cliked = true;

                J3_Tiempo2();

            }
            BP2 = true;
        }
        public void j3_MetodoPaletas3()
        {

            if (j3_Clicked == false)
            {

                j3_bolsa1Cliked = true;

                J3_Tiempo2();

            }
            BP3 = true;
        }
        public void j3_MetodoBolsa1()
        {

            if (j3_Clicked == false)
            {

                j3_bolsa1Cliked = true;

                J3_Tiempo2();

            }
            BB1 = true;

            j3_OK1();
        }
        public void j3_OK1()
        {

            if (BP2 == true && BB1 == true)
            {

                OK2 = true;

                j3_Bolsa1 = "j3_BolsaLlibreLlibre.png";

                j3_Finalitzar();

            }
            else if (BP1 == true || BP3 == true)
            {

                j3_fatalerrorAsync();

            }

            BP1 = false;
            BP2 = false;
            BP3 = false;

            BB1 = false;
            BB2 = false;
            BB3 = false;

        }
        public void j3_MetodoBolsa2()
        {

            if (j3_Clicked == false)
            {

                j3_bolsa1Cliked = true;

                J3_Tiempo2();

            }
            BB2 = true;
            j3_OK2();
        }
        public void j3_OK2()
        {

            if (BP3 == true && BB2 == true)
            {

                OK3 = true;

                j3_Bolsa2 = "j3_BolsaBoliBoli.png";

                j3_Finalitzar();

            }
            else if (BP2 == true || BP3 == true)
            {

                j3_fatalerrorAsync();

            }

            BP1 = false;
            BP2 = false;
            BP3 = false;

            BB1 = false;
            BB2 = false;
            BB3 = false;

        }
        public void j3_MetodoBolsa3()
        {

            if (j3_Clicked == false)
            {

                j3_bolsa1Cliked = true;

                J3_Tiempo2();

            }
            BB3 = true;

            j3_OK3();

        }
        public void j3_OK3()
        {

            if (BP1 == true && BB3 == true)
            {

                OK1 = true;

                j3_Bolsa3 = "j3_BolsaBoliLlibre.png";

                j3_Finalitzar();

            }
            else if (BP2 == true || BP3 == true)
            {

                j3_fatalerrorAsync();

            }

            BP1 = false;
            BP2 = false;
            BP3 = false;

            BB1 = false;
            BB2 = false;
            BB3 = false;

        }
        public void j3_Finalitzar()
        {

            if (OK1 == true && OK2 == true && OK3 == true)
            {

                j3_win();
                j3_Punts = 20000;

            }

        }
        async Task J3_Tiempo2()
        {


            while (j3_tiempo > 0)
            {

                await Task.Delay(1000);


                j3_tiempo = j3_tiempo - 1;

            }



            if (j3_tiempo == 0)
            {

                j3_TimeOut();

            }

        }
        public async System.Threading.Tasks.Task j3_TimeOut()
        {

            await Application.Current.MainPage.DisplayAlert("Temps finalitzat", "Puntuació = 0", "Seguent joc");

        }
        public async System.Threading.Tasks.Task j3_fatalerrorAsync()
        {

            await Application.Current.MainPage.DisplayAlert("Error", "Has fallat.", "Salir");
            PasarJuego3 = true;
            j3_Punts = 0;
        }
        public async System.Threading.Tasks.Task j3_win()
        {

            await Application.Current.MainPage.DisplayAlert("Has guanyat", "Puntuació = 20000", "Seguent joc");
            PasarJuego3 = true;
        }
        //----------- JUEGO 4 -----------//
        public void j4_PosiRandom()
        {
            //Hace random la posicion en la que la imagen aparece en la pantalla
            j4_X = j4_randum.Next(-500, 350);
            j4_Y = j4_randum.Next(-600, 320);
        }
        public void j4_points()
        {

            j4_puntosF = j4_puntosF + j4_puntos;

        }
        public void j4_Speak()
        {
            // Funcion encargada de actualizar el texto del relor
            j4_Timer = j4_min.ToString() + " : " + j4_seg.ToString("0,0");
        }
        public async void j4_MetodoError()
        {
            if (j4_tiempoF == 0)
            {

                while (j4_stop)
                {
                    j4_stop = false;

                    await Application.Current.MainPage.DisplayAlert("Fin", "Puntuacion\n" + j4_puntos.ToString(), "Continuar");

                    await Task.Delay(1000);

                }
            }
        }
        public async Task j4_Stop()
        {
            //Funcion que sirve para hacer que el planeta no se salga de la pantalla
            while (true)
            {
                if ((j4_X <= 345 && j4_X >= -635) && (j4_Y <= 370 && j4_Y >= -540))
                {
                    j4_Go = true;
                }
                else
                {
                    j4_Go = false;
                }

                await Task.Delay(1);
            }
        }
        public async Task j4_Reloj()
        {
            //Funcion basica que sirve de reloj
            while (true)
            {
                if (j4_seg == 59)
                {
                    j4_seg = -1;
                    j4_min = j4_min + 1;
                }

                j4_seg = j4_seg + 1;
                j4_Speak();
                await Task.Delay(1000);
                j4_tiempoF = j4_tiempoF - 1;
            }
        }
        public async Task j4_Puntuador()
        {
            //Funcion que calcula los j4_puntos segun la posicon en la que se encuentre el planeta

            while (true)
            {
                if ((j4_X <= -145 && j4_X >= -165) && (j4_Y > -15 && j4_Y < 15))
                {
                    j4_puntos = j4_puntos + 5;

                    j4_points();

                    j4_Puntuacion = "Puntos : " + j4_puntos.ToString();
                }
                else
                {
                    j4_puntos = j4_puntos + 0;

                    j4_Puntuacion = "Puntos : " + j4_puntos.ToString();
                }

                await Task.Delay(1000);
            }

        }
        //----------- JUEGO 5 -----------//
        public async void j5_Afirmativo()
        {
            if (j5_rc == j5_rt)   // j5_si 
            {
                j5_Si++;
                j5_Puntos += 1;
                j5_conta_afir = true;
            }
            else
            {
                j5_No++;
                j5_Puntos -= 2;
                j5_conta_afir = false;
            }

            j5_BotoSi = "j5_verde1.png";
            await Task.Delay(200);
            j5_BotoSi = "j5_verde2.png";
            await Task.Delay(250);
            j5_BotoSi = "j5_verde1.png";
        }
        public async void j5_Negativo()
        {
            if (j5_rc == j5_rt)
            {
                j5_No++;
                j5_Puntos -= 2;
                j5_conta_afir = false;
            }
            else
            {
                j5_Si++;
                j5_Puntos += 1;
                j5_conta_afir = true;
                j5_Puntos += 1;
                j5_conta_afir = true;
            }

            j5_BotoNo = "j5_rojo1.png";
            await Task.Delay(200);
            j5_BotoNo = "j5_rojo2.png";
            await Task.Delay(250);
            j5_BotoNo = "j5_rojo1.png";
        }
        async Task j5_Colores()
        {
            Random j5_random = new Random();
            while (true)
            {
                j5_contestada = false;
                j5_rc = j5_random.Next(6);  // variable j5_random para cambiar colores
                j5_rt = j5_random.Next(6); // variable j5_random para cambiar texto

                if (j5_rt != j5_rc)   //comprobacion que siempre haya 50% de que coincida el texto con el color
                {
                    j5_sincro++;   // variable para la sincronizacion
                    if (j5_sincro == 2)   //en caso de que j5_sincro sea =2 
                    {
                        j5_rt = j5_rc;      // j5_rt sea igual a j5_rc osea el texto sea igual al color
                        j5_sincro = 0;  //volver a poner la variable de sincronizar en 0
                    }
                }

                switch (j5_rc) //en caso de que j5_random color j5_rc sea igual al 0
                {

                    case 0:
                        j5_Color1 = "Cyan";  //variable de color que sea cyan
                        break;
                    case 1:
                        j5_Color1 = "Red";
                        break;
                    case 2:
                        j5_Color1 = "Violet";
                        break;
                    case 3:
                        j5_Color1 = "Lime";
                        break;
                    case 4:
                        j5_Color1 = "Orange";
                        break;
                    case 5:
                        j5_Color1 = "White";
                        break;

                }

                switch (j5_rt)
                {
                    case 0:
                        j5_Text_color = "AZUL";  //variable de color que sea cyan
                        break;
                    case 1:
                        j5_Text_color = "ROJO";
                        break;
                    case 2:
                        j5_Text_color = "MORADO";
                        break;
                    case 3:
                        j5_Text_color = "VERDE";
                        break;
                    case 4:
                        j5_Text_color = "NARANJA";
                        break;
                    case 5:
                        j5_Text_color = "BLANCO";
                        break;

                }

                await Task.Delay(1500);
                if (j5_conta_afir == true)   //funcion para bonus boolean
                {
                    j5_Contador_afir += 1;
                    if (j5_Contador_afir == 8)  //contador para el bonus j5_si j5_contador_afir llega a 8 que haga lo de abajo
                    {
                        j5_Puntos += 5;   // sumando +5 j5_puntos
                        j5_Contador_afir = 0;   //despues de sumar pongo otra vez el j5_contador_afir 
                    }
                }
                if (j5_conta_afir == false)    // j5_si j5_conta_afir es falso
                {
                    j5_Contador_afir = 0;  //poner contador de contestadas en racha en 0
                }
                if (j5_contestada == false)  // en caso de que j5_contestada sea false
                {
                    j5_Puntos -= 1;   //resta 250puntos
                    j5_conta_afir = false;  // poniendo j5_conta_afir en false
                }

            }  // finsih while

        }
        //----------- JUEGO 6 -----------//
        public void j6_Speak()
        {
            // Funcion encargada de actualizar el texto del relor
            j6_Timer = j6_min.ToString() + " : " + j6_seg.ToString("0,0");
        }
        public async Task j6_Reloj()
        {
            //Funcion basica que sirve de reloj
            while (true)
            {
                if (j6_seg == 59)
                {
                    j6_seg = -1;
                    j6_min = j6_min + 1;
                }

                j6_seg = j6_seg + 1;
                j6_Speak();
                await Task.Delay(1000);
            }
        }
        //----------- JUEGO 7 -----------//
        public void j7_Carta1()
        {
            j7_numeroClick++; //Se suma uno al numero de clicks

            j7_ImatgeCarta1 = j7_ArrayCartes[0];
            j7_estatcarta1 = true;

            j7_ComprovarCartes(j7_ImatgeCarta1); //Entra en la funcion de comprovar cartas
            j7_ActivarCarta1 = false;

            if (j7_tiempobool == false)
            {
                j7_Temps();
                j7_tiempobool = true;
            }

        }
        public void j7_Carta2()
        {
            j7_numeroClick++;

            j7_ImatgeCarta2 = j7_ArrayCartes[1];
            j7_estatcarta2 = true;

            j7_ComprovarCartes(j7_ImatgeCarta2);
            j7_ActivarCarta2 = false;

            if (j7_tiempobool == false)
            {
                j7_Temps();
                j7_tiempobool = true;
            }

        }
        public void j7_Carta3()
        {
            j7_numeroClick++;

            j7_ImatgeCarta3 = j7_ArrayCartes[2];
            j7_estatcarta3 = true;

            j7_ComprovarCartes(j7_ImatgeCarta3);
            j7_ActivarCarta3 = false;

            if (j7_tiempobool == false)
            {
                j7_Temps();
                j7_tiempobool = true;
            }

        }
        public void j7_Carta4()
        {
            j7_numeroClick++;

            j7_ImatgeCarta4 = j7_ArrayCartes[3];
            j7_estatcarta4 = true;

            j7_ComprovarCartes(j7_ImatgeCarta4);
            j7_ActivarCarta4 = false;

            if (j7_tiempobool == false)
            {
                j7_Temps();
                j7_tiempobool = true;
            }
        }
        public void j7_Carta5()
        {
            j7_numeroClick++;

            j7_ImatgeCarta5 = j7_ArrayCartes[4];
            j7_estatcarta5 = true;

            j7_ComprovarCartes(j7_ImatgeCarta5);
            j7_ActivarCarta5 = false;

            if (j7_tiempobool == false)
            {
                j7_Temps();
                j7_tiempobool = true;
            }
        }
        public void j7_Carta6()
        {
            j7_numeroClick++;

            j7_ImatgeCarta6 = j7_ArrayCartes[5];
            j7_estatcarta6 = true;

            j7_ComprovarCartes(j7_ImatgeCarta6);
            j7_ActivarCarta6 = false;

            if (j7_tiempobool == false)
            {
                j7_Temps();
                j7_tiempobool = true;
            }
        }
        public void j7_Carta7()
        {
            j7_numeroClick++;

            j7_ImatgeCarta7 = j7_ArrayCartes[6];
            j7_estatcarta7 = true;

            j7_ComprovarCartes(j7_ImatgeCarta7);
            j7_ActivarCarta7 = false;

            if (j7_tiempobool == false)
            {
                j7_Temps();
                j7_tiempobool = true;
            }
        }
        public void j7_Carta8()
        {
            j7_numeroClick++;

            j7_ImatgeCarta8 = j7_ArrayCartes[7];
            j7_estatcarta8 = true;
            j7_ComprovarCartes(j7_ImatgeCarta8);
            j7_ActivarCarta8 = false;

            if (j7_tiempobool == false)
            {
                j7_Temps();
                j7_tiempobool = true;
            }

        }
        public void j7_Carta9()
        {
            j7_numeroClick++;

            j7_ImatgeCarta9 = j7_ArrayCartes[8];
            j7_estatcarta9 = true;

            j7_ComprovarCartes(j7_ImatgeCarta9);
            j7_ActivarCarta9 = false;

            if (j7_tiempobool == false)
            {
                j7_Temps();
                j7_tiempobool = true;
            }

        }
        public void j7_Carta10()
        {
            j7_numeroClick++;

            j7_ImatgeCarta10 = j7_ArrayCartes[9];
            j7_estatcarta10 = true;

            j7_ComprovarCartes(j7_ImatgeCarta10);
            j7_ActivarCarta10 = false;

           

        }
        public void j7_Carta11()
        {
            j7_numeroClick++;

            j7_ImatgeCarta11 = j7_ArrayCartes[10];
            j7_estatcarta11 = true;

            j7_ComprovarCartes(j7_ImatgeCarta11);
            j7_ActivarCarta11 = false;

           

        }
        public void j7_Carta12()
        {
            j7_numeroClick++;

            j7_ImatgeCarta12 = j7_ArrayCartes[11];
            j7_estatcarta12 = true;

            j7_ComprovarCartes(j7_ImatgeCarta12);
            j7_ActivarCarta12 = false;

           


        }
        public void j7_RandomCartes()
        {
            Random rnd = new Random();

            //Random para asignar un numero a cada carta
            List<int> j7_listNumbers = new List<int>();
            int j7_number;
            for (int j7_i = 0; j7_i < 8; j7_i++)
            {
                do
                {
                    j7_number = rnd.Next(8);
                } while (j7_listNumbers.Contains(j7_number));
                j7_listNumbers.Add(j7_number);
            }
            //Random para asignar una imagen a cada carta
            List<int> j7_listFotos = new List<int>();
            int j7_fotos;
            for (int j7_i = 0; j7_i < 19; j7_i++)
            {
                do
                {
                    j7_fotos = rnd.Next(19);
                } while (j7_listFotos.Contains(j7_fotos));
                j7_listFotos.Add(j7_fotos);
            }

            //Random para hacer dos cartas iguales
            for (int j7_i = 0; j7_i < 8; j7_i = j7_i + 2)
            {
                j7_numrandom = rnd.Next(19);

                j7_ArrayCartes[j7_listNumbers[j7_i]] = j7_ArrayFotos[j7_listFotos[j7_i]];
                j7_ArrayCartes[j7_listNumbers[j7_i + 1]] = j7_ArrayFotos[j7_listFotos[j7_i]];
            }

        }
        public void j7_RandomCartes2()
        {
            Random rnd = new Random();

            //Random para asignar un numero a cada carta
            List<int> j7_listNumbers = new List<int>();
            int j7_number;
            for (int j7_i = 0; j7_i < 12; j7_i++)
            {
                do
                {
                    j7_number = rnd.Next(12);
                } while (j7_listNumbers.Contains(j7_number));
                j7_listNumbers.Add(j7_number);
            }

            //Random para asignar una imagen a cada carta
            List<int> j7_listFotos = new List<int>();
            int j7_fotos;
            for (int j7_i = 0; j7_i < 19; j7_i++)
            {
                do
                {
                    j7_fotos = rnd.Next(19);
                } while (j7_listFotos.Contains(j7_fotos));
                j7_listFotos.Add(j7_fotos);
            }

            //Random para hacer dos cartas iguales
            for (int j7_i = 0; j7_i < 12; j7_i = j7_i + 2)
            {
                j7_numrandom = rnd.Next(19);

                j7_ArrayCartes[j7_listNumbers[j7_i]] = j7_ArrayFotos[j7_listFotos[j7_i]];
                j7_ArrayCartes[j7_listNumbers[j7_i + 1]] = j7_ArrayFotos[j7_listFotos[j7_i]];
            }

        }
        public void j7_Reiniciar()
        {
            //Funcion que se ejecuta al hacer click en el boton de reiniciar
            j7_TextMostrar = "MEMORY";
            //Se vuelven a poner todas las cartas giradas como al principio
            j7_ImatgeCarta1 = "j7_cartareves.png";
            j7_ImatgeCarta2 = "j7_cartareves.png";
            j7_ImatgeCarta3 = "j7_cartareves.png";
            j7_ImatgeCarta4 = "j7_cartareves.png";
            j7_ImatgeCarta5 = "j7_cartareves.png";
            j7_ImatgeCarta6 = "j7_cartareves.png";
            j7_ImatgeCarta7 = "j7_cartareves.png";
            j7_ImatgeCarta8 = "j7_cartareves.png";
            j7_ImatgeCarta9 = "j7_cartareves.png";
            j7_ImatgeCarta10 = "j7_cartareves.png";
            j7_ImatgeCarta11 = "j7_cartareves.png";
            j7_ImatgeCarta12 = "j7_cartareves.png";
            //Se vuelven a activar todas las cartas
            j7_ActivarCarta1 = true;
            j7_ActivarCarta2 = true;
            j7_ActivarCarta3 = true;
            j7_ActivarCarta4 = true;
            j7_ActivarCarta5 = true;
            j7_ActivarCarta6 = true;
            j7_ActivarCarta7 = true;
            j7_ActivarCarta8 = true;
            j7_ActivarCarta9 = true;
            //j7_ActivarCarta10 = true;
            //j7_ActivarCarta11 = true;
            //j7_ActivarCarta12 = true;
            //j7_CartaVisible1 = true;
            //j7_CartaVisible2 = true;
            //j7_CartaVisible3 = true;
            //j7_CartaVisible4 = true;
            //Se oculta el boton de reiniciar
            j7_VisibleReiniciar = false;
            //Se reinician variables contador
            j7_numeroClick = 0;
            j7_contgirades = 0;
            //Se aumenta al segundo j7_nivel
            j7_nivel = 6;
            j7_flag = true;
            j7_Tiempo = 30;
            //Entra en la funcion del segundo j7_nivel
            //j7_RandomCartes2();

        }
        public void j7_ComprovarCartes(String j7_CartaEntrada)
        {
            //En la funcion entra el src (string) de la carta
            //Con esta condicion y el j7_flag se guarda a otra variable para poder comprobar cada dos veces
            if (j7_flag == true)
            {
                j7_valorar1 = j7_CartaEntrada;
                j7_flag = false;
            }
            else
            {
                j7_valorar2 = j7_CartaEntrada;
                j7_flag = true;
            }
            //Cuando el numero de clicks sea igual a dos, las cartas se desactivan (no hacen nada si las tocas)
            if (j7_numeroClick == 2)
            {
                j7_ActivarCarta1 = false;
                j7_ActivarCarta2 = false;
                j7_ActivarCarta3 = false;
                j7_ActivarCarta4 = false;
                j7_ActivarCarta5 = false;
                j7_ActivarCarta6 = false;
                j7_ActivarCarta7 = false;
                j7_ActivarCarta8 = false;
                j7_ActivarCarta9 = false;
                j7_ActivarCarta10 = false;
                j7_ActivarCarta11 = false;
                j7_ActivarCarta12 = false;

                //Si las cartas son diferentes
                if (j7_valorar1 != j7_valorar2)
                {
                    j7_TextMostrar = "OH! VUELVE A INTENTARLO!";
                    j7_Unsegon(); //Pausa de un segundo
                    j7_contgirades = 0; //No se ha girado ninguna carta

                    //Nunca el numero de puntos será inferior a zero
                    if (j7_puntuacio_flag > 0)
                    {
                        //Resta mil puntos porque se ha fallado
                        j7_puntuacio_flag = j7_puntuacio_flag - 1000;
                        j7_puntuacio = j7_puntuacio_flag;
                    }

                }
                else
                {
                    //Si las cartas son iguales
                    j7_TextMostrar = "QUE BUENA!";
                    j7_contgirades++;
                    //Suma mil puntos porque son iguales
                    j7_puntuacio_flag = j7_puntuacio_flag + 1000;
                    j7_puntuacio = j7_puntuacio_flag;
                    //Se activan todas las cartas
                    j7_ActivarCarta1 = true;
                    j7_ActivarCarta2 = true;
                    j7_ActivarCarta3 = true;
                    j7_ActivarCarta4 = true;
                    j7_ActivarCarta5 = true;
                    j7_ActivarCarta6 = true;
                    j7_ActivarCarta7 = true;
                    j7_ActivarCarta8 = true;
                    j7_ActivarCarta9 = true;
                    j7_ActivarCarta10 = true;
                    j7_ActivarCarta11 = true;
                    j7_ActivarCarta12 = true;
                }
                j7_numeroClick = 0; //Se reinicializan los clicks
            }

            //j7_nivel superado
            if (j7_contgirades == j7_nivel)
            {
                //Cuando el numero de cartas giradas es igual al numero de j7_nivel
                j7_TextMostrar = "NIVEL SUPERADO!";
                //Se activa el boton de reiniciar
                j7_VisibleReiniciar = true;
                //Se desactivan todas las cartas (no hacen nada si las tocas)
                j7_ActivarCarta1 = false;
                j7_ActivarCarta2 = false;
                j7_ActivarCarta3 = false;
                j7_ActivarCarta4 = false;
                j7_ActivarCarta5 = false;
                j7_ActivarCarta6 = false;
                j7_ActivarCarta7 = false;
                j7_ActivarCarta8 = false;
                j7_ActivarCarta9 = false;
                j7_ActivarCarta10 = false;
                j7_ActivarCarta11 = false;
                j7_ActivarCarta12 = false;
                Application.Current.MainPage.DisplayAlert("Felicidades!", "Obtienes 20.000 puntos", "Continuar");
                j7_puntuacio = 20000;
                jr_Puntos_ranking = j1_Puntos + j2_Punts + j3_Punts + j7_Puntuacio;
            }

        }
        public async Task j7_Unsegon()
        {
            //Función para hacer una pausa de un segundo
            await Task.Delay(1000);
            //Se vuelven a poner todas las cartas giradas como al principio
            j7_ImatgeCarta1 = "j7_cartareves.png";
            j7_ImatgeCarta2 = "j7_cartareves.png";
            j7_ImatgeCarta3 = "j7_cartareves.png";
            j7_ImatgeCarta4 = "j7_cartareves.png";
            j7_ImatgeCarta5 = "j7_cartareves.png";
            j7_ImatgeCarta6 = "j7_cartareves.png";
            j7_ImatgeCarta7 = "j7_cartareves.png";
            j7_ImatgeCarta8 = "j7_cartareves.png";
            j7_ImatgeCarta9 = "j7_cartareves.png";
            j7_ImatgeCarta10 = "j7_cartareves.png";
            j7_ImatgeCarta11 = "j7_cartareves.png";
            j7_ImatgeCarta12 = "j7_cartareves.png";
            //Se activan las cartas
            j7_ActivarCarta1 = true;
            j7_ActivarCarta2 = true;
            j7_ActivarCarta3 = true;
            j7_ActivarCarta4 = true;
            j7_ActivarCarta5 = true;
            j7_ActivarCarta6 = true;
            j7_ActivarCarta7 = true;
            j7_ActivarCarta8 = true;
            j7_ActivarCarta9 = true;
            j7_ActivarCarta10 = true;
            j7_ActivarCarta11 = true;
            j7_ActivarCarta12 = true;
        }
        public async Task j7_Temps()
        {
            while (j7_VisibleReiniciar == false)
            {
                await Task.Delay(1000);

                j7_Tiempo--;

                if (j7_Tiempo == 0)
                {
                    j7_VisibleReiniciar = true;
                    j7_Puntuacio = 0;
                    await Application.Current.MainPage.DisplayAlert("Noooo!", "Se ha acabado el tiempo obtienes 0 puntos", "Continuar");
                    jr_Puntos_ranking = j1_Puntos + j2_Punts + j3_Punts + j7_Puntuacio;
                    j7_ActivarCarta1 = false;
                    j7_ActivarCarta2 = false;
                    j7_ActivarCarta3 = false;
                    j7_ActivarCarta4 = false;
                    j7_ActivarCarta5 = false;
                    j7_ActivarCarta6 = false;
                    j7_ActivarCarta7 = false;
                    j7_ActivarCarta8 = false;
                    j7_ActivarCarta9 = false;
                    j7_ActivarCarta10 = false;
                    j7_ActivarCarta11 = false;
                    j7_ActivarCarta12 = false;
                }
            }

        }
        //----------- LIMPIAR PROYECTO -----------//
        //----------- RANKING -----------//
        public void Basededatos()
        {
            Controlador controlador = new Controlador();

            List<Jugador> listaControlador = new List<Jugador>();

            listaControlador = Controlador.ObtenerJugadores();

            controlador.Usuarios = jr_Nombre;
            controlador.Puntuaciones = jr_Puntos_ranking;

            Controlador.AgregarJugador(jr_Nombre, jr_Puntos_ranking);
        }

    }
}